package com.backendadminportalnle.controller;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.backendadminportalnle.model.MST_PLATFORM;
import com.backendadminportalnle.model.TT_PROFILE_PLATFORM;
import com.backendadminportalnle.repository.MST_PLATFORM_REPO;
import com.backendadminportalnle.repository.TT_PROFILE_PLATFORM_REPO;
import com.backendadminportalnle.repository.TT_USERS_REPO;
import com.backendadminportalnle.service.MST_PLATFORM_SERVICE;
import com.fasterxml.jackson.core.JsonProcessingException;

@CrossOrigin(allowedHeaders = "*", origins = "*")
@RestController
@RequestMapping(value="/api/v1/")
public class MST_PLATFORM_CONTROLLER {
	String status = "status";
	String success = "Success";
	String failed = "Fail";
	Timestamp today = new Timestamp(System.currentTimeMillis());
	@Autowired
	private MST_PLATFORM_REPO platformRepo;
	
	@Autowired
	private MST_PLATFORM_SERVICE platformService;
	
	@Autowired
	private TT_PROFILE_PLATFORM_REPO profilePlatformRepo;
	
	@Autowired
	private TT_USERS_REPO userRepo;
	
	@PostMapping(value="/platforms")
	private ResponseEntity<?> save(@RequestBody MST_PLATFORM item){
		Map<String, Object> body = new HashMap<>();
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			Integer user_id = this.userRepo.findUserIdByUsername(auth.getName());	
			MST_PLATFORM data = this.platformService.save(item, user_id, today);
			body.put("status", "success");
			body.put("data", data);
			return new ResponseEntity<>(body,HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value="/platforms/{platform_id}")
	private ResponseEntity<?> edit(@PathVariable("platform_id")String platform_id,@RequestBody MST_PLATFORM item) throws JsonProcessingException{
		Map<String, Object> body = new HashMap<>();
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			Integer user_id = this.userRepo.findUserIdByUsername(auth.getName());
			MST_PLATFORM dataSave = this.platformService.edit(item, platform_id, user_id, today);
			body.put(status, success);
			body.put("data", dataSave);
			return new ResponseEntity<>(body, HttpStatus.OK); 
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="/platforms")
	private ResponseEntity<?> getByCategoryServiceCms(@RequestParam(required = false) Integer categoryservices_id,
			  @RequestParam(defaultValue = "", required = false)String platform_id,
			  @RequestParam(defaultValue = "",required = false)String platform_name,@RequestParam(defaultValue = "1") int page,
		      @RequestParam(defaultValue = "10") int size, @RequestParam (defaultValue = "platform_id")String sort,@RequestParam (defaultValue = "asc")String order){
		Map<String, Object> body = new HashMap<>();
		if(categoryservices_id==null) {
			Map<String, Object> data = platformService.getAllByPlatformCms(platform_id, platform_name,page,size,sort,order);
			if(data.isEmpty()) {
				body.put(status, HttpStatus.BAD_REQUEST.value());
				body.put("message", "Data tidak ditemukan");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			}else {
				body.put(status,HttpStatus.OK.value());
				body.put("message", success);
				body.put("data", data);
				return new ResponseEntity<>(body,HttpStatus.OK);
			}			
		}else {
			categoryservices_id = categoryservices_id==null?0:categoryservices_id;
			Map<String, Object> data = platformService.getByCategoryServiceCms(categoryservices_id, platform_id, platform_name,page,size,sort,order);
			if(data.isEmpty()) {
				body.put(status, HttpStatus.BAD_REQUEST.value());
				body.put("message", "Data tidak ditemukan");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			}else {
				body.put(status,HttpStatus.OK.value());
				body.put("message", success);
				body.put("data", data);
				return new ResponseEntity<>(body,HttpStatus.OK);
			}			
		}
	}
	
	@GetMapping(value="/platforms/{platform_id}")
	private ResponseEntity<?> getByIdCms(@PathVariable("platform_id")String platform_id){
		Map<String, Object> body = new HashMap<>();
		MST_PLATFORM dataFindByid = this.platformService.getByidCms(platform_id);
		if(dataFindByid!=null) {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", dataFindByid);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}else {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="/public/platforms")
	private ResponseEntity<?> getByCategoryServicePortal(@RequestParam(required = false) Integer categoryservices_id,
			  @RequestParam(defaultValue = "", required = false)String platform_id,
			  @RequestParam(defaultValue = "",required = false)String platform_name,@RequestParam(defaultValue = "1") int page,
		      @RequestParam(defaultValue = "10") int size, @RequestParam (defaultValue = "platform_id")String sort,@RequestParam (defaultValue = "asc")String order){
		Map<String, Object> body = new HashMap<>();
		if(categoryservices_id==null) {
			Map<String, Object> data = platformService.getAllByPlatformPublic(platform_id, platform_name,page,size,sort,order);
			if(data.isEmpty()) {
				body.put(status, HttpStatus.BAD_REQUEST.value());
				body.put("message", "Data tidak ditemukan");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			}else {
				body.put(status,HttpStatus.OK.value());
				body.put("message", success);
				body.put("data", data);
				return new ResponseEntity<>(body,HttpStatus.OK);
			}			
		}else {
			categoryservices_id = categoryservices_id==null?0:categoryservices_id;
			Map<String, Object> data = platformService.getByCategoryServiceCms(categoryservices_id, platform_id, platform_name,page,size,sort,order);
			if(data.isEmpty()) {
				body.put(status, HttpStatus.BAD_REQUEST.value());
				body.put("message", "Data tidak ditemukan");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			}else {
				body.put(status,HttpStatus.OK.value());
				body.put("message", success);
				body.put("data", data);
				return new ResponseEntity<>(body,HttpStatus.OK);
			}			
		}
	}
	
	@GetMapping(value="/public/platforms/{platform_id}")
	private ResponseEntity<?> getByIdPortal(@PathVariable("platform_id")String platform_id){
		Map<String, Object> body = new HashMap<>();
		MST_PLATFORM dataFindByid = this.platformService.getByidPublic(platform_id);
		if(dataFindByid!=null) {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", dataFindByid);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}else {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
}
