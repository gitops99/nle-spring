package com.backendadminportalnle.service;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.backendadminportalnle.exception.FileStorageException;
import com.backendadminportalnle.exception.MyFileNotFoundException;
import com.backendadminportalnle.model.FileDB;
import com.backendadminportalnle.property.FileStorageProperties;
import com.backendadminportalnle.repository.FileDBRepository;


@Service
public class FileStorageService {
	@Autowired
	private FileDBRepository fileDBRepository;
	
	private Sort.Direction getSortDirection(String direction) {
	    if (direction.equals("asc")||direction.equals("ASC")) {
	      return Sort.Direction.ASC;
	    } else if (direction.equals("desc")||direction.equals("DESC")) {
	      return Sort.Direction.DESC;
	    }
	
	    return Sort.Direction.ASC;
	}
	
	private final Path fileStorageLocation;
	
	@Autowired
	public FileStorageService(FileStorageProperties fileStorageProperties) {
		this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
				.toAbsolutePath().normalize();
		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception e) {
			throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", e);
		}
	}
	
	private static final List<String> contentTypes = Arrays.asList(
			"image/png", "image/jpeg", "image/gif",
			"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", 
			"application/vnd.openxmlformats-officedocument.wordprocessingml.template",
			"application/pdf", 
			"application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
			"application/vnd.openxmlformats-officedocument.spreadsheetml.template","text/csv",
			"application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation"
			);
	
	private static String getLocationDirectory = null;
	
	public String storeFile(MultipartFile file, String idFile, Date dt) {
		// Normalize file name
		String fileName = idFile+"."+StringUtils.cleanPath(file.getOriginalFilename()
				.substring(file.getOriginalFilename().lastIndexOf(".")+1));
		String contentType = file.getContentType();
		try {
			System.out.println("idFile : "+idFile);
			// Check if the file's name contains invalid characters
			if(contentTypes.contains(contentType)) {
				// Copy file to the target location (Replacing existing file with the same name)
				Path h1 = this.fileStorageLocation;
				Path h2 = Paths.get("fileUpload/"+(dt.getYear()+1900)+"/"+(dt.getMonth()+1)+"/");
				Path targetLocation = h1.resolveSibling(h2).resolve(fileName);
				if (!Files.exists(h2)) {
					Files.createDirectories(h2);
				}
				System.out.println("fileName = "+fileName);
				getLocationDirectory = targetLocation.toString();
		        System.out.println(getLocationDirectory);
				Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
			} else {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
			}
			
			return fileName;
		} catch (Exception e) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", e);
		}
	}
	public Map<String, Object> storeFileNews(MultipartFile file, String idFile, Date dt,String idFile2) {
		Map<String, Object> response = new HashMap<>();
		String getLocationDirectory1 = null;
		String getLocationDirectory2 = null;
		// Normalize file name
		String fileName = idFile+"."+StringUtils.cleanPath(file.getOriginalFilename()
				.substring(file.getOriginalFilename().lastIndexOf(".")+1));
		String fileName2 = idFile2+"."+StringUtils.cleanPath(file.getOriginalFilename()
				.substring(file.getOriginalFilename().lastIndexOf(".")+1));
		String contentType = file.getContentType();
		int scaledWidth = 350;
        int scaledHeight = 210;
        int originalWidth = 750;
        int originalHeight = 450;
		try {
			System.out.println("idFile : "+idFile);
			System.out.println("idFile2 : "+idFile2);
			// Check if the file's name contains invalid characters
			if(contentTypes.contains(contentType)) {
				// Copy file to the target location (Replacing existing file with the same name)
				Path h1 = this.fileStorageLocation;
				Path h2 = Paths.get("fileUpload/"+(dt.getYear()+1900)+"/"+(dt.getMonth()+1)+"/");
				Path targetLocation = h1.resolveSibling(h2).resolve(fileName);
				Path targetLocation2 = h1.resolveSibling(h2).resolve(fileName2);
				if (!Files.exists(h2)) {
					Files.createDirectories(h2);
				}
				System.out.println("fileName = "+fileName);
				System.out.println("fileName2 = "+fileName2);
				getLocationDirectory1 = targetLocation.toString();
				getLocationDirectory2 = targetLocation2.toString();
		        System.out.println(getLocationDirectory1);
		        System.out.println(getLocationDirectory2);
				Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
				Files.copy(file.getInputStream(), targetLocation2, StandardCopyOption.REPLACE_EXISTING);
				FileStorageService.resize(getLocationDirectory1, getLocationDirectory1, scaledWidth, scaledHeight);
				FileStorageService.resize(getLocationDirectory2, getLocationDirectory2, originalWidth, originalHeight);
				response.put("FileName1", fileName);
				response.put("FileName2", fileName2);
			} else {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
			}
			
			return response;
		} catch (Exception e) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", e);
		}
	}
	public Resource loadFileAsResource(String fileName,Date dt) throws Exception{
		try {
			Path h1 = this.fileStorageLocation;
			String h2 = "fileUpload/"+(dt.getYear()+1900)+"/"+(dt.getMonth()+1)+"/";
			Path h = h1.resolveSibling(h2).resolve(fileName).normalize();
			
			Resource resource = new UrlResource(h.toUri());
			System.out.println("load file = "+this.fileStorageLocation+"\npath edit = "+h);
			if(resource.exists()) {
				return resource;
			} else {
				throw new MyFileNotFoundException("File not found " + fileName);
			}
		} catch (MalformedURLException e) {
			throw new MyFileNotFoundException("File not found " + fileName, e);
		}
	}
	
	public String deleteFile(String fileName, Date dt) {
		try {
			if(fileName == null) {
				throw new FileStorageException("Sorry! Filename contains null " + fileName);
			} else {
				// Copy file to the target location (Replacing existing file with the same name)
				Path h1 = this.fileStorageLocation;
				String h2 = "fileUpload/"+(dt.getYear()+1900)+"/"+(dt.getMonth()+1)+"/";
				Path targetLocation = h1.resolveSibling(h2).resolve(fileName).normalize();
				
				Files.deleteIfExists(targetLocation);
//				return resource;
				return "Delete filename "+fileName+" is success.";
			}
		} catch (Exception e) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", e);
		}
	}
	
	public Map<String, Object> getAllPagesByTypeDecs(String file_type, String file_desc,int page,int size, String sort,String order){
		Map<String, Object> response = new HashMap<>();
		List<FileDB> findByTypeDesc = new ArrayList<FileDB>();
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(getSortDirection(order), sort));
		Pageable pagingSort = PageRequest.of(page-1, size, Sort.by(orders));
		Page<FileDB> pageTuts;
		pageTuts = this.fileDBRepository.getAllPage(file_type, file_desc,pagingSort);
		findByTypeDesc = pageTuts.getContent();
		response.put("data", findByTypeDesc);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	public static void resize(String inputImagePath,
            String outputImagePath, int scaledWidth, int scaledHeight)
            throws IOException {
        // reads input image
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);
 
        // creates output image
        BufferedImage outputImage = new BufferedImage(scaledWidth,
                scaledHeight, inputImage.getType());
 
        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();
 
        // extracts extension of output file
        String formatName = outputImagePath.substring(outputImagePath
                .lastIndexOf(".") + 1);
 
        // writes to output file
        ImageIO.write(outputImage, formatName, new File(outputImagePath));
    }
	
	public static void resize(String inputImagePath,
            String outputImagePath, double percent) throws IOException {
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);
        int scaledWidth = (int) (inputImage.getWidth() * percent);
        int scaledHeight = (int) (inputImage.getHeight() * percent);
        resize(inputImagePath, outputImagePath, scaledWidth, scaledHeight);
    }
}
