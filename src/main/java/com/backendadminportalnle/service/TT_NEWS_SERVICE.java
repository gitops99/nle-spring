package com.backendadminportalnle.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.backendadminportalnle.model.FileDB;
import com.backendadminportalnle.model.TT_NEWS;
import com.backendadminportalnle.repository.FileDBRepository;
import com.backendadminportalnle.repository.TT_NEWS_REPO;

@Service
public class TT_NEWS_SERVICE {
	@Autowired
	private TT_NEWS_REPO tt_NEWS_REPO;
	
	@Autowired
	private FileDBRepository fileDBRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	private Sort.Direction getSortDirection(String direction) {
	    if (direction.equals("asc")||direction.equals("ASC")) {
	      return Sort.Direction.ASC;
	    } else if (direction.equals("desc")||direction.equals("DESC")) {
	      return Sort.Direction.DESC;
	    }
	
	    return Sort.Direction.ASC;
	}
	
	@Cacheable("datafindById")
	public TT_NEWS getByidPublic(Integer news_id) {
		return this.tt_NEWS_REPO.findbyId(news_id);
	}
	
	public TT_NEWS getByidCms(Integer news_id) {
		return this.tt_NEWS_REPO.findbyId(news_id);
	}
	
	@Cacheable("dataTTNews")
	public Map<String, Object> getTTNewsPublic(List<Integer> categorynews_id,Integer news_id,String search, int page,int size, String sort,String order){
		Map<String, Object> response = new HashMap<>();
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(getSortDirection(order), sort));
		List<TT_NEWS> data = new ArrayList<TT_NEWS>();
		Pageable pagingSort = PageRequest.of(page-1, size, Sort.by(orders));		
		Page<TT_NEWS> pageTuts;
		pageTuts = this.tt_NEWS_REPO.findbyCategoryNewsId(categorynews_id,news_id,search, pagingSort);
		data = pageTuts.getContent();
		
		response.put("data", data);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	public Map<String, Object> getTTNewsCms(List<Integer> categorynews_id,Integer news_id,String search, int page,int size, String sort,String order){
		Map<String, Object> response = new HashMap<>();
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(getSortDirection(order), sort));
		List<TT_NEWS> data = new ArrayList<TT_NEWS>();
		Pageable pagingSort = PageRequest.of(page-1, size, Sort.by(orders));		
		Page<TT_NEWS> pageTuts;
		pageTuts = this.tt_NEWS_REPO.findbyCategoryNewsId(categorynews_id,news_id,search, pagingSort);
		data = pageTuts.getContent();
		
		response.put("data", data);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	@Cacheable("dataTTNews")
	public Map<String, Object> getAllTTNewsPublic(Integer news_id,String search, int page,int size, String sort,String order){
		Map<String, Object> response = new HashMap<>();
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(getSortDirection(order), sort));
		List<TT_NEWS> data = new ArrayList<TT_NEWS>();
		Pageable pagingSort = PageRequest.of(page-1, size, Sort.by(orders));		
		Page<TT_NEWS> pageTuts;
		pageTuts = this.tt_NEWS_REPO.findAllPage(news_id,search, pagingSort);
		data = pageTuts.getContent();
		
		response.put("data", data);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	public Map<String, Object> getAllTTNewsCms(Integer news_id,String search, int page,int size, String sort,String order){
		Map<String, Object> response = new HashMap<>();
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(getSortDirection(order), sort));
		List<TT_NEWS> data = new ArrayList<TT_NEWS>();
		Pageable pagingSort = PageRequest.of(page-1, size, Sort.by(orders));		
		Page<TT_NEWS> pageTuts;
		pageTuts = this.tt_NEWS_REPO.findAllPage(news_id,search, pagingSort);
		data = pageTuts.getContent();
		
		response.put("data", data);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
		
	@CacheEvict(value = "dataTTNews", allEntries = true)
	public TT_NEWS save(TT_NEWS item, Integer user_id,Timestamp today,MultipartFile thumbnail,String file_desc,HttpServletRequest request) {
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String date = simpleDateFormat.format(new Date());
		
		UUID uuid = UUID.randomUUID();
		String idFile = date+"-"+uuid.toString();
		Date dt = new Date();

		UUID uuid2 = UUID.randomUUID();
		String idFile2 = date+"-"+uuid2.toString();
		
		Map<String, Object> fileName = fileStorageService.storeFileNews(thumbnail, idFile, dt,idFile2);
		String fileName1 = (String) fileName.get("FileName1");
		String fileName2 = (String) fileName.get("FileName2");
		String uploadViewUrl1 = request.getScheme()+"://"+ request.getServerName()+ ":"+request.getServerPort()+"/upload/viewFile/"+ fileName1;
		String uploadViewUrl2 = request.getScheme()+"://"+ request.getServerName()+ ":"+request.getServerPort()+"/upload/viewFile/"+ fileName2;
//		Save DB 
		FileDB dataSaveDb1 = new FileDB(thumbnail.getOriginalFilename(),uploadViewUrl1, thumbnail.getContentType(),file_desc,fileName1, today);
		this.fileDBRepository.save(dataSaveDb1);
		FileDB dataSaveDb2 = new FileDB(thumbnail.getOriginalFilename(),uploadViewUrl2, thumbnail.getContentType(),file_desc,fileName2, today);
		this.fileDBRepository.save(dataSaveDb2);
		
		item.setThumbnail(uploadViewUrl1);
		item.setOriginal_thumbnail(uploadViewUrl2);
		item.setCreated_by(user_id);
		item.setUpdated_by(user_id);
		item.setCreated_at(today);
		item.setUpdated_at(today);
		item.setSlug(item.getTitle().toLowerCase().replace(" ", "-"));
		TT_NEWS data = this.tt_NEWS_REPO.save(item);
		return data;
	}
	
	@CacheEvict(value = "dataTTNews", allEntries = true)
	public TT_NEWS edit(Integer news_id,TT_NEWS item, Integer user_id,Timestamp today,MultipartFile thumbnail,String file_desc,HttpServletRequest request) throws ParseException {
		String thumbnailSearch = this.tt_NEWS_REPO.findThumbnail(news_id);
		Integer idFileDB = this.fileDBRepository.findIdByURL(thumbnailSearch);
		if(this.fileDBRepository.findById(idFileDB).isPresent()) {
			String fileName = this.fileDBRepository.findFileName(idFileDB);
			String date = fileName.substring(0, 10);
			Date dt = new SimpleDateFormat("yyyy-MM-dd").parse(date);
			fileStorageService.deleteFile(fileName, dt);
			this.fileDBRepository.deleteById(idFileDB);
		}
		String originalThumbnail = this.tt_NEWS_REPO.findOriginalThumbnail(news_id);
		Integer idFileDBOriginalThumbnail = this.fileDBRepository.findIdByURL(originalThumbnail);
		if(this.fileDBRepository.findById(idFileDBOriginalThumbnail).isPresent()) {
			String fileName2 = this.fileDBRepository.findFileName(idFileDBOriginalThumbnail);
			String date = fileName2.substring(0, 10);
			Date dt = new SimpleDateFormat("yyyy-MM-dd").parse(date);
			fileStorageService.deleteFile(fileName2, dt);
			this.fileDBRepository.deleteById(idFileDBOriginalThumbnail);
		}
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String date = simpleDateFormat.format(new Date());
		
		UUID uuid = UUID.randomUUID();
		String idFile = date+"-"+uuid.toString();
		Date dt = new Date();
		
		UUID uuid2 = UUID.randomUUID();
		String idFile2 = date+"-"+uuid2.toString();
		
		Map<String, Object> fileName = fileStorageService.storeFileNews(thumbnail, idFile, dt,idFile2);
		String fileName1 = (String) fileName.get("FileName1");
		String fileName2 = (String) fileName.get("FileName2");
		String uploadViewUrl1 = request.getScheme()+"://"+ request.getServerName()+ ":"+request.getServerPort()+"/upload/viewFile/"+ fileName1;
		String uploadViewUrl2 = request.getScheme()+"://"+ request.getServerName()+ ":"+request.getServerPort()+"/upload/viewFile/"+ fileName2;
//		Save DB 
		FileDB dataSaveDb1 = new FileDB(thumbnail.getOriginalFilename(),uploadViewUrl1, thumbnail.getContentType(),file_desc,fileName1, today);
		this.fileDBRepository.save(dataSaveDb1);
		FileDB dataSaveDb2 = new FileDB(thumbnail.getOriginalFilename(),uploadViewUrl2, thumbnail.getContentType(),file_desc,fileName2, today);
		this.fileDBRepository.save(dataSaveDb2);
		
		TT_NEWS data = this.tt_NEWS_REPO.findById(news_id).get();
		data.setUpdated_by(user_id);
		data.setUpdated_at(today);
		data.setTitle(item.getTitle());
		data.setThumbnail(uploadViewUrl1);
		data.setOriginal_thumbnail(uploadViewUrl2);
		data.setDescription(item.getDescription());
		data.setLink(item.getLink());
		data.setTag(item.getTag());
		data.setSlug(item.getTitle().toLowerCase().replace(" ","-"));
		data.setCategory_news(item.getCategory_news());
		TT_NEWS dataSave = this.tt_NEWS_REPO.save(data);
		return dataSave;
	}
	
	@CacheEvict(value = "dataTTNews", allEntries = true)
	public Map<String, Object> delete(Integer news_id){
		this.tt_NEWS_REPO.deleteById(news_id);
		Map<String, Object> response = new HashMap<>();
		response.put("status", "Success");
		response.put("message", "News has been deleted successfully");
		return response;
	}
	
	public List<TT_NEWS> find2BycategoryNews(){
		return this.tt_NEWS_REPO.find2BycategoryNews();
	}
}
