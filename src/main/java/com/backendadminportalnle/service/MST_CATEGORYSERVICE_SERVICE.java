package com.backendadminportalnle.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.backendadminportalnle.model.MST_CATEGORYSERVICE;
import com.backendadminportalnle.repository.MST_CATEGORYSERVICE_REPO;

@Service
public class MST_CATEGORYSERVICE_SERVICE {
	@Autowired
	private MST_CATEGORYSERVICE_REPO categoryservice_REPO;
	
	private Sort.Direction getSortDirection(String direction) {
	    if (direction.equals("asc")||direction.equals("ASC")) {
	      return Sort.Direction.ASC;
	    } else if (direction.equals("desc")||direction.equals("DESC")) {
	      return Sort.Direction.DESC;
	    }
	
	    return Sort.Direction.ASC;
	}
	
	@Cacheable("dataByCategoryServiceId")
	public MST_CATEGORYSERVICE findByidPortal(Integer categoryservice_id) {
		return this.categoryservice_REPO.findbyId(categoryservice_id);
	}
	
	public MST_CATEGORYSERVICE findByidCms(Integer categoryservice_id) {
		return this.categoryservice_REPO.findbyId(categoryservice_id);
	}
	
	@Cacheable("dataAllCategoryServicePortal")
	public Map<String, Object> findAllPortal(Integer categoryservice_id, String category_name,int page,int size, String sort,String order){
		Map<String, Object> response = new HashMap<>();
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(getSortDirection(order), sort));
		List<MST_CATEGORYSERVICE> data = new ArrayList<MST_CATEGORYSERVICE>();
		Pageable pagingSort = PageRequest.of(page-1, size, Sort.by(orders));
		Page<MST_CATEGORYSERVICE> pageTuts;
		pageTuts = this.categoryservice_REPO.findAllPage(categoryservice_id, category_name, pagingSort);
		data = pageTuts.getContent();
		
		response.put("data", data);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	public Map<String, Object> findAllCms(Integer categoryservice_id, String category_name,int page,int size, String sort,String order){
		Map<String, Object> response = new HashMap<>();
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(getSortDirection(order), sort));
		List<MST_CATEGORYSERVICE> data = new ArrayList<MST_CATEGORYSERVICE>();
		Pageable pagingSort = PageRequest.of(page-1, size, Sort.by(orders));
		Page<MST_CATEGORYSERVICE> pageTuts;
		pageTuts = this.categoryservice_REPO.findAllPage(categoryservice_id, category_name, pagingSort);
		data = pageTuts.getContent();
		
		response.put("data", data);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	@CacheEvict(value = "dataAllCategoryServicePortal", allEntries = true)
	public MST_CATEGORYSERVICE save(MST_CATEGORYSERVICE item,Timestamp today) {
		item.setCreated_at(today);
		item.setUpdated_at(today);
		MST_CATEGORYSERVICE data = this.categoryservice_REPO.save(item);
		return data;
	}
	
	@CacheEvict(value = "dataAllCategoryServicePortal", allEntries = true)
	public MST_CATEGORYSERVICE edit(Integer categoryservice_id,MST_CATEGORYSERVICE item,Timestamp today) {
		MST_CATEGORYSERVICE data = this.categoryservice_REPO.findById(categoryservice_id).get();
		data.setUpdated_at(today);
		data.setCategory_name(item.getCategory_name());
		data.setDescription(item.getDescription());
		data.setIcon(item.getIcon());
		data.setIs_from(item.getIs_from());
		data.setIs_show(item.getIs_show());
		data.setUrl_service(item.getUrl_service());
		MST_CATEGORYSERVICE resp = this.categoryservice_REPO.save(data);
		return resp;
	}
	
	@CacheEvict(value = "dataAllCategoryServicePortal", allEntries = true)
	public Map<String, Object> delete(Integer categoryservice_id) {
		this.categoryservice_REPO.deleteById(categoryservice_id);
		Map<String, Object> response = new HashMap<>();
		response.put("status", "Success");
		response.put("message", "Category Service has been deleted successfully");
		return response;
	}
}
