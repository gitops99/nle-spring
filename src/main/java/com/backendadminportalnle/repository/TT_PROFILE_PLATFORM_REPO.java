package com.backendadminportalnle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.backendadminportalnle.model.TT_PROFILE_PLATFORM;

@Repository
public interface TT_PROFILE_PLATFORM_REPO extends JpaRepository<TT_PROFILE_PLATFORM, String>{

}
