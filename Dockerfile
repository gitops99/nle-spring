# START OF DEVELOPMENT
FROM maven:3.6.3-openjdk-11 AS maven_build

WORKDIR /home/backend-admin-portal-nle
COPY . .

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.6.0/wait /wait
RUN chmod +x /wait

EXPOSE 8045

CMD mvn spring-boot:run
# END OF DEVELOPMENT
