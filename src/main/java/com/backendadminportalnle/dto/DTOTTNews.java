package com.backendadminportalnle.dto;

import java.util.List;

import com.backendadminportalnle.model.MST_CATEGORYNEWS;

public class DTOTTNews {
	
	private String title;
	private String thumbnail;
	private String description;
	private String link;
	private String slug;
	private String tag;
	List<MST_CATEGORYNEWS> category_news;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public List<MST_CATEGORYNEWS> getCategory_news() {
		return category_news;
	}
	public void setCategory_news(List<MST_CATEGORYNEWS> category_news) {
		this.category_news = category_news;
	}
	
}
