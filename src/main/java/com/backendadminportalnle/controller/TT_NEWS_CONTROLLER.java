package com.backendadminportalnle.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.backendadminportalnle.model.FileDB;
import com.backendadminportalnle.model.TT_NEWS;
import com.backendadminportalnle.repository.FileDBRepository;
import com.backendadminportalnle.repository.TT_NEWS_REPO;
import com.backendadminportalnle.repository.TT_USERS_REPO;
import com.backendadminportalnle.service.FileStorageService;
import com.backendadminportalnle.service.TT_NEWS_SERVICE;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin(allowedHeaders = "*", origins = "*")
@RestController
@RequestMapping(value="/api/v1/")
public class TT_NEWS_CONTROLLER {
	String status = "status";
	String success = "Success";
	String failed = "Fail";
	Timestamp today = new Timestamp(System.currentTimeMillis());
	@Autowired
	private TT_NEWS_REPO tt_NEWS_REPO;
	
	@Autowired
	private TT_USERS_REPO userRepo;
	
	@Autowired
	private TT_NEWS_SERVICE newsService;
	
	@Autowired
	private FileDBRepository fileDBRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@PostMapping(value="/news")
	private ResponseEntity<?> saveMultiPart(@RequestPart("news")String news, 
											@RequestPart("thumbnail")MultipartFile thumbnail,
											@RequestPart("file_desc")String file_desc,HttpServletRequest request){
		Map<String, Object> body = new HashMap<>();
		TT_NEWS newsJson = new TT_NEWS();
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			newsJson = objectMapper.readValue(news, TT_NEWS.class);
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			Integer user_id = this.userRepo.findUserIdByUsername(auth.getName());
			TT_NEWS data = this.newsService.save(newsJson, user_id, today,thumbnail,file_desc,request);

			body.put(status, success);
			body.put("data", data);
			return new ResponseEntity<>(body, HttpStatus.OK); 
		} catch (Exception e) {
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value="/news/{news_id}")
	private ResponseEntity<?> editNews(@PathVariable("news_id")Integer news_id,@RequestPart("news")String news, 
			@RequestPart("thumbnail")MultipartFile thumbnail,
			@RequestPart("file_desc")String file_desc,
			HttpServletRequest request){
		Map<String, Object> body = new HashMap<>();
		TT_NEWS newsJson = new TT_NEWS();
		try {
			if(this.tt_NEWS_REPO.findById(news_id).isPresent()) {
				ObjectMapper objectMapper = new ObjectMapper();
				newsJson = objectMapper.readValue(news, TT_NEWS.class);
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				Integer user_id = this.userRepo.findUserIdByUsername(auth.getName());
				TT_NEWS dataSave = this.newsService.edit(news_id, newsJson, user_id, today,thumbnail,file_desc,request);
				body.put(status, success);
				body.put("data", dataSave);
				return new ResponseEntity<>(body, HttpStatus.OK); 
			}else{
				body.put(status, failed );
				body.put("message", "Data news null");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);	
			}
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@DeleteMapping(value="/news/{news_id}")
	private ResponseEntity<?> delete(@PathVariable("news_id")Integer news_id){
		Map<String, Object> bodyDelete = new HashMap<>();
		try {
			TT_NEWS dataExsisting = this.tt_NEWS_REPO.findbyId(news_id);
			String url = dataExsisting.getThumbnail();
			Integer idFile = this.fileDBRepository.findIdByURL(url);
			if(idFile!=null) {
				if(this.fileDBRepository.findById(idFile).isPresent()) {
					String fileName = this.fileDBRepository.findFileName(idFile);
					String date = fileName.substring(0, 10);
					Date dt = new SimpleDateFormat("yyyy-MM-dd").parse(date);
					fileStorageService.deleteFile(fileName, dt);
					this.fileDBRepository.deleteById(idFile);				
				}				
			}
			this.newsService.delete(news_id);
			bodyDelete.put(status, success);
			bodyDelete.put("message", "News has been deleted successfully");
			return new ResponseEntity<>(bodyDelete,HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			bodyDelete.put(status, failed);
			bodyDelete.put("message", "Delete news failed");
			return new ResponseEntity<>(bodyDelete,HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="/news")
	private ResponseEntity<?>getCategoryNewsCms(@RequestParam(required = false)List<Integer> categorynews_id,
												@RequestParam(defaultValue = "", required = false)String search,
												@RequestParam(required=false)Integer news_id,@RequestParam(defaultValue = "1") int page,
											      @RequestParam(defaultValue = "100") int size, @RequestParam (defaultValue = "created_at")String sort,@RequestParam (defaultValue = "desc")String order){
		Map<String, Object> body = new HashMap<>();
		if(categorynews_id==null) {
			news_id = news_id==null?0:news_id;
			Map<String, Object> data = newsService.getAllTTNewsCms(news_id,search,page,size,sort,order );
			if(data.isEmpty()) {
				body.put(status, HttpStatus.BAD_REQUEST.value());
				body.put("message", "Data tidak ditemukan");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			}else {
				body.put(status,HttpStatus.OK.value());
				body.put("message", success);
				body.put("data", data);
				return new ResponseEntity<>(body,HttpStatus.OK);
			}
		}else {
			news_id = news_id==null?0:news_id;
			Map<String, Object> data = newsService.getTTNewsCms(categorynews_id,news_id,search,page,size,sort,order );
			if(data.isEmpty()) {
				body.put(status, HttpStatus.BAD_REQUEST.value());
				body.put("message", "Data tidak ditemukan");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			}else {
				body.put(status,HttpStatus.OK.value());
				body.put("message", success);
				body.put("data", data);
				return new ResponseEntity<>(body,HttpStatus.OK);
			}
		}
	}
	
	@GetMapping(value="/news/{news_id}")
	private ResponseEntity<?> getByIdCms(@PathVariable("news_id")Integer news_id){
		Map<String, Object> body = new HashMap<>();
		TT_NEWS dataFindByid = this.newsService.getByidCms(news_id);
		if(dataFindByid!=null) {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", dataFindByid);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}else {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="/public/news")
	private ResponseEntity<?>getCategoryNewsPublic(@RequestParam(required = false)List<Integer> categorynews_id,
			@RequestParam(defaultValue = "", required = false)String search,
			@RequestParam(required=false)Integer news_id,@RequestParam(defaultValue = "1") int page,
		      @RequestParam(defaultValue = "100") int size, @RequestParam (defaultValue = "created_at")String sort,@RequestParam (defaultValue = "desc")String order){
		Map<String, Object> body = new HashMap<>();
		List<Integer> list=Arrays.asList(0,0);
		categorynews_id = categorynews_id==null?list:categorynews_id;
		if(categorynews_id.isEmpty()) {
			news_id = news_id==null?0:news_id;
			Map<String, Object> data = newsService.getAllTTNewsCms(news_id,search,page,size,sort,order );
			if(data.isEmpty()) {
				body.put(status, HttpStatus.BAD_REQUEST.value());
				body.put("message", "Data tidak ditemukan");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			}else {
				body.put(status,HttpStatus.OK.value());
				body.put("message", success);
				body.put("data", data);
				return new ResponseEntity<>(body,HttpStatus.OK);
			}
		}else {
			news_id = news_id==null?0:news_id;
			Map<String, Object> data = newsService.getTTNewsCms(categorynews_id,news_id,search,page,size,sort,order );
			if(data.isEmpty()) {
				body.put(status, HttpStatus.BAD_REQUEST.value());
				body.put("message", "Data tidak ditemukan");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			}else {
				body.put(status,HttpStatus.OK.value());
				body.put("message", success);
				body.put("data", data);
				return new ResponseEntity<>(body,HttpStatus.OK);
			}
		}
	}
	
	@GetMapping(value="/public/news/{news_id}")
	private ResponseEntity<?> getByIdPublic(@PathVariable("news_id")Integer news_id){
		Map<String, Object> body = new HashMap<>();
		TT_NEWS dataFindByid = this.newsService.getByidPublic(news_id);
		if(dataFindByid!=null) {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", dataFindByid);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}else {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="/public/news/landing")
	private ResponseEntity<?> gettest(){
		Map<String, Object> body = new HashMap<>();
		List<TT_NEWS> getFind2BycategoryNews = this.newsService.find2BycategoryNews();
		if(getFind2BycategoryNews!=null) {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", getFind2BycategoryNews);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}else {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
}
