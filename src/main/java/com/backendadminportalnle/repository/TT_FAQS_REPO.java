package com.backendadminportalnle.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.backendadminportalnle.model.TT_FAQS;

@Repository
public interface TT_FAQS_REPO extends JpaRepository<TT_FAQS, Integer>{
	@Query(value="SELECT * FROM TT_FAQS WHERE (FAQ_ID =:faq_id OR :faq_id = 0)",nativeQuery =true)
	Page<TT_FAQS> getDataAll(@Param("faq_id")Integer faq_id,Pageable pageable);
	
	@Query(value="SELECT * FROM TT_FAQS WHERE FAQ_ID =:faq_id",nativeQuery =true)
	TT_FAQS getDataByFaqId(@Param("faq_id")Integer faq_id);
}
