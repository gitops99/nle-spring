package com.backendadminportalnle.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="mst_platform")
public class MST_PLATFORM {
	@Id
	@Column(name="platform_id", length = 5)
	private String platform_id;

	@Column(name="platform_name", nullable = false)
	private String platform_name;
	
	@Column(name="company", nullable = false)
	private String company;
	
	@Column(name="npwp", length = 30)
	private String npwp;
	
	@Column(name="platform_url")
	private String platform_url;
	
	@Column(name="thumbnail")
	private String thumbnail;
	
	@JsonManagedReference
	@OneToOne(cascade = CascadeType.ALL,mappedBy = "platform")
//    @PrimaryKeyJoinColumn
	private TT_PROFILE_PLATFORM profile_platform;
	
	@ManyToMany
	@JoinTable
	(
      name="tr_categories_platforms",
      joinColumns={ @JoinColumn(name="platform_id") },
      inverseJoinColumns={ @JoinColumn(name="categoryservice_id")}
	)
	private List<MST_CATEGORYSERVICE> category_services;
	
	public TT_PROFILE_PLATFORM getProfile_platform() {
		return profile_platform;
	}

	public void setProfile_platform(TT_PROFILE_PLATFORM profile_platform) {
		this.profile_platform = profile_platform;
	}

	public List<MST_CATEGORYSERVICE> getCategory_services() {
		return category_services;
	}

	public void setCategory_services(List<MST_CATEGORYSERVICE> category_services) {
		this.category_services = category_services;
	}

	public String getPlatform_id() {
		return platform_id;
	}

	public void setPlatform_id(String platform_id) {
		this.platform_id = platform_id;
	}

	public String getPlatform_name() {
		return platform_name;
	}

	public void setPlatform_name(String platform_name) {
		this.platform_name = platform_name;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public String getPlatform_url() {
		return platform_url;
	}

	public void setPlatform_url(String platform_url) {
		this.platform_url = platform_url;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

}
