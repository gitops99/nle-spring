package com.backendadminportalnle.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.backendadminportalnle.model.TT_FAQS;
import com.backendadminportalnle.repository.TT_FAQS_REPO;

@Service
public class TT_FAQS_SERVICE {
	@Autowired
	private TT_FAQS_REPO tt_FAQS_REPO;
	
	private Sort.Direction getSortDirection(String direction) {
	    if (direction.equals("asc")||direction.equals("ASC")) {
	      return Sort.Direction.ASC;
	    } else if (direction.equals("desc")||direction.equals("DESC")) {
	      return Sort.Direction.DESC;
	    }
	
	    return Sort.Direction.ASC;
	}
	
	@Cacheable("dataByidTTFaqs")
	public TT_FAQS getByIdPublic(Integer faqs_id) {
		return this.tt_FAQS_REPO.getDataByFaqId(faqs_id);
	}
	
	public TT_FAQS getByIdCms(Integer faqs_id) {
		return this.tt_FAQS_REPO.getDataByFaqId(faqs_id);
	}
	
	@Cacheable("dataAllTTFaqs")
	public Map<String, Object> getTTFaqsPublic(Integer faqs_id,int page,int size, String sort,String order){
		Map<String, Object> response = new HashMap<>();
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(getSortDirection(order), sort));
		List<TT_FAQS> data = new ArrayList<TT_FAQS>();
		Pageable pagingSort = PageRequest.of(page-1, size, Sort.by(orders));
		Page<TT_FAQS> pageTuts;
		pageTuts = this.tt_FAQS_REPO.getDataAll(faqs_id,pagingSort);
		data = pageTuts.getContent();
		
		response.put("data", data);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	public Map<String, Object> getTTFaqsCms(Integer faqs_id,int page,int size, String sort,String order){
		Map<String, Object> response = new HashMap<>();
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(getSortDirection(order), sort));
		List<TT_FAQS> data = new ArrayList<TT_FAQS>();
		Pageable pagingSort = PageRequest.of(page-1, size, Sort.by(orders));
		Page<TT_FAQS> pageTuts;
		pageTuts = this.tt_FAQS_REPO.getDataAll(faqs_id,pagingSort);
		data = pageTuts.getContent();
		
		response.put("data", data);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	@CacheEvict(value = "dataAllTTFaqs", allEntries = true)
	public TT_FAQS save(TT_FAQS item, Timestamp today) {
		item.setCreated_at(today);
		item.setUpdated_at(today);
		TT_FAQS data = this.tt_FAQS_REPO.save(item);
		return data;
	}
	
	@CacheEvict(value = "dataAllTTFaqs", allEntries = true)
	public TT_FAQS edit(Integer faq_id,TT_FAQS item,Timestamp today) {
		TT_FAQS tt_faqs = this.tt_FAQS_REPO.findById(faq_id).get();
		tt_faqs.setUpdated_at(today);
		tt_faqs.setDescription(item.getDescription());
		tt_faqs.setTitle(item.getTitle());
		TT_FAQS data = this.tt_FAQS_REPO.save(tt_faqs);
		return data;
	}
	
	@CacheEvict(value = "dataAllTTFaqs", allEntries = true)
	public Map<String, Object> delete(Integer faq_id) {
		this.tt_FAQS_REPO.deleteById(faq_id);
		Map<String, Object> response = new HashMap<>();
		response.put("status", "Success");
		response.put("message", "Category News has been deleted successfully");
		return response;
	}
}
