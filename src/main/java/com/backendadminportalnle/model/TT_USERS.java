package com.backendadminportalnle.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonSetter;

@Entity
@Table(name="tt_users")
@SequenceGenerator(name= "seq_tt_users", sequenceName = "SEQ_ID_USERS", initialValue=1, allocationSize = 1)
public class TT_USERS {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seq_tt_users")
	@Column(name="users_id")
	private Integer users_id;
	
	@Column(name="role_id")
	private Integer role_id;
	
	@Column(name="username",length = 255, nullable = false)
	private String username;
	
	@JsonIgnore
	@JsonSetter
	@Column(name="password", nullable = true)
	private String password;
	
	@Column(name="email",length = 255, nullable = false)
	private String email;
	
	@Column(name="fullname",length = 255, nullable = false)
	private String fullname;
	
	@Column(name="token",length = 255)
	private String token;
	
	@Column(name="reset_token",length = 255)
	private String reset_token;

	@Column(name="npwp",length = 50, nullable = true)
	private String npwp;

	@Column(name="is_approved", nullable = true)
	private Boolean is_approved;

	@Column(name="created_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp created_at;
	
	@Column(name="updated_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp updated_at;

	@ManyToOne
	@JoinColumn(name="role_id", foreignKey = @ForeignKey(name="fk_id_role"),insertable = false, updatable = false)
	@JsonProperty("role")
	private MST_ROLE mst_role;
	
	public Integer getUsers_id() {
		return users_id;
	}

	public void setUsers_id(Integer users_id) {
		this.users_id = users_id;
	}

	public Boolean getIs_approved() {
		return is_approved;
	}

	public void setIs_approved(Boolean is_approved) {
		this.is_approved = is_approved;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getReset_token() {
		return reset_token;
	}

	public void setReset_token(String reset_token) {
		this.reset_token = reset_token;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}

	public Integer getRole_id() {
		return role_id;
	}

	public void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}

	public MST_ROLE getMst_role() {
		return mst_role;
	}

	public void setMst_role(MST_ROLE mst_role) {
		this.mst_role = mst_role;
	}

}
