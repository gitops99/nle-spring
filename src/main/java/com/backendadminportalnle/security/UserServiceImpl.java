package com.backendadminportalnle.security;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.backendadminportalnle.model.TT_USERS;
import com.backendadminportalnle.repository.TT_USERS_REPO;

@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService{
	@Autowired
	private TT_USERS_REPO userRepo;
	
	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		TT_USERS user = userRepo.findbyUsername(username);
		if(user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new User(user.getUsername(),user.getPassword(),getAuthority(user));
	}
	
	private Set<SimpleGrantedAuthority> getAuthority(TT_USERS user){
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		authorities.add(new SimpleGrantedAuthority(user.getMst_role().getRole_name()));
		return authorities;
	}
}
