package com.backendadminportalnle.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name="mst_categorynews")
@SequenceGenerator(name= "seq_mst_categorynews", sequenceName = "SEQ_ID_CATEGORYNEWS", initialValue=1, allocationSize = 1)
public class MST_CATEGORYNEWS {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_mst_categorynews")
	@Column(name="category_news_id")
	private Integer category_news_id;
	
	@Column(name="category_name",length = 255, nullable = false)
	private String category_name;
	
	@Column(name="created_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp created_at;
	
	@Column(name="updated_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp updated_at;
	
	public Integer getCategory_news_id() {
		return category_news_id;
	}

	public void setCategory_news_id(Integer category_news_id) {
		this.category_news_id = category_news_id;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}
}
