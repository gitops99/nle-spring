package com.backendadminportalnle.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="mst_categoryservice")
@SequenceGenerator(name= "seq_mst_categoryservice", sequenceName = "SEQ_ID_CATEGORYSERVICE", initialValue=1, allocationSize = 1)
public class MST_CATEGORYSERVICE {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_mst_categoryservice")
	@Column(name="categoryservice_id")
	private Integer categoryservice_id;
	
	@Column(name="category_name",length = 255, nullable = false)
	private String category_name;
	
	@Column(name="description", columnDefinition = "CLOB")
	private String description;
	
	@Column(name="is_from", columnDefinition = "integer default 1")
	private Integer is_from;
	
	@Column(name="is_show", columnDefinition = "integer default 1")
	private Integer is_show;
	
	@Column(name="icon", columnDefinition = "CLOB", nullable = false)
	private String icon;
	
	@Column(name="url_service", columnDefinition = "CLOB", nullable = false)
	private String url_service;
	
	@Column(name="created_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp created_at;
	
	@Column(name="updated_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp updated_at;

	public Integer getCategoryservice_id() {
		return categoryservice_id;
	}

	public void setCategoryservice_id(Integer categoryservice_id) {
		this.categoryservice_id = categoryservice_id;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getIs_from() {
		return is_from;
	}

	public void setIs_from(Integer is_from) {
		this.is_from = is_from;
	}

	public Integer getIs_show() {
		return is_show;
	}

	public void setIs_show(Integer is_show) {
		this.is_show = is_show;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getUrl_service() {
		return url_service;
	}

	public void setUrl_service(String url_service) {
		this.url_service = url_service;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}

}
