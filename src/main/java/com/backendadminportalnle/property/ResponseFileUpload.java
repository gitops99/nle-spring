package com.backendadminportalnle.property;

import org.springframework.web.multipart.MultipartFile;

public class ResponseFileUpload {
private String idFile;
	
	private String filename;
	
	private String file;
	
	public ResponseFileUpload(String idFile2, MultipartFile file2) {}
	
	public ResponseFileUpload(String idFile, String filename, String file) {
		this.idFile = idFile;
		this.filename = filename;
		this.file = file;
	}

	public String getIdFile() {
		return idFile;
	}

	public void setIdFile(String idFile) {
		this.idFile = idFile;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}
}
