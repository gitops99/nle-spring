package com.backendadminportalnle.controller;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.backendadminportalnle.model.MST_CATEGORYSERVICE;
import com.backendadminportalnle.repository.MST_CATEGORYSERVICE_REPO;
import com.backendadminportalnle.service.MST_CATEGORYSERVICE_SERVICE;

@CrossOrigin(allowedHeaders = "*", origins = "*")
@RestController
@RequestMapping(value="/api/v1/")
public class MST_CATEGORYSERVICE_CONTROLLER {
	String status = "status";
	String success = "Success";
	String failed = "Fail";
	Timestamp today = new Timestamp(System.currentTimeMillis());
	@Autowired
	private MST_CATEGORYSERVICE_REPO categoryservice_REPO;
	
	@Autowired
	private MST_CATEGORYSERVICE_SERVICE categoryService;

	@PostMapping(value="/categoryservice")
	private ResponseEntity<?> save(@RequestBody MST_CATEGORYSERVICE item){
		Map<String, Object> body = new HashMap<>();
		try {
			MST_CATEGORYSERVICE data = this.categoryService.save(item, today);
			body.put(status, success);
			body.put("data", data);
			return new ResponseEntity<>(body, HttpStatus.OK); 
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value="/categoryservice/{categoryservice_id}")
	private ResponseEntity<?> edit(@PathVariable("categoryservice_id")Integer categoryservice_id,@RequestBody MST_CATEGORYSERVICE item){
		Map<String, Object> body = new HashMap<>();
		try {
			if(this.categoryservice_REPO.findById(categoryservice_id).isPresent()) {
				MST_CATEGORYSERVICE data = this.categoryService.edit(categoryservice_id, item, today);
				body.put(status, success);
				body.put("message", "Category Service has been updated successfully");
				return new ResponseEntity<>(body, HttpStatus.OK); 
			}else {
				body.put(status, failed );
				body.put("message", "Data category news null");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value="/categoryservice/{categoryservice_id}")
	private ResponseEntity<?> delete(@PathVariable("categoryservice_id") Integer categoryservice_id){
		Map<String, Object> bodyDelete = new HashMap<>();
		try {
			this.categoryService.delete(categoryservice_id);
			bodyDelete.put(status, success);
			bodyDelete.put("message", "Category Service has been deleted successfully");
			return new ResponseEntity<>(bodyDelete,HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			bodyDelete.put(status, failed);
			bodyDelete.put("message", "Delete Category Service failed");
			return new ResponseEntity<>(bodyDelete,HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="/categoryservice")
	private ResponseEntity<?> getAllCms(@RequestParam(required = false)Integer categoryservice_id,
			@RequestParam(defaultValue = "",required = false)String category_name,@RequestParam(defaultValue = "1") int page,
		      @RequestParam(defaultValue = "10") int size, @RequestParam (defaultValue = "categoryservice_id")String sort,@RequestParam (defaultValue = "asc")String order){
		Map<String, Object> body = new HashMap<>();
		categoryservice_id = categoryservice_id==null?0:categoryservice_id;
		Map<String, Object> data = this.categoryService.findAllCms(categoryservice_id, category_name, page,size,sort,order);
		if(data.isEmpty()) {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}else {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", data);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/categoryservice/{categoryservice_id}")
	private ResponseEntity<?> getByidCms(@PathVariable("categoryservice_id")Integer categoryservice_id){
		Map<String, Object> body = new HashMap<>();
		MST_CATEGORYSERVICE dataFindbyId = this.categoryService.findByidCms(categoryservice_id);
		if(dataFindbyId!=null) {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", dataFindbyId);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}else {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="/public/categoryservice")
	private ResponseEntity<?> getAllPublic(@RequestParam(required = false)Integer categoryservice_id,
			@RequestParam(defaultValue = "",required = false)String category_name,@RequestParam(defaultValue = "1") int page,
		      @RequestParam(defaultValue = "10") int size, @RequestParam (defaultValue = "categoryservice_id")String sort,@RequestParam (defaultValue = "asc")String order){
		Map<String, Object> body = new HashMap<>();
		categoryservice_id = categoryservice_id==null?0:categoryservice_id;
		Map<String, Object> data = this.categoryService.findAllPortal(categoryservice_id, category_name, page,size,sort,order);
		if(data.isEmpty()) {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}else {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", data);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/public/categoryservice/{categoryservice_id}")
	private ResponseEntity<?> getByidPublic(@PathVariable("categoryservice_id")Integer categoryservice_id){
		Map<String, Object> body = new HashMap<>();
		MST_CATEGORYSERVICE dataFindbyId = this.categoryService.findByidPortal(categoryservice_id);
		if(dataFindbyId!=null) {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", dataFindbyId);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}else {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
}
