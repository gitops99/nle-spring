package com.backendadminportalnle.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.backendadminportalnle.model.FileDB;
import com.backendadminportalnle.property.ResponseFileUpload;
import com.backendadminportalnle.repository.FileDBRepository;
import com.backendadminportalnle.service.FileStorageService;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin(allowedHeaders = "*", origins = "*")
@RestController
@RequestMapping(value="/upload")
public class UPLOAD_FILE_CONTROLLER {
	String status = "status";
	String success = "Success";
	String failed = "Fail";
	Timestamp today = new Timestamp(System.currentTimeMillis());
	@Autowired
	private FileDBRepository fileDBRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	final ObjectMapper mapper = new ObjectMapper();
	@PostMapping(value="/file", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	private ResponseEntity<?> saveFile(@RequestPart ("file") MultipartFile file,@RequestParam String file_desc, HttpServletRequest request) {
		Map<String, Object> body = new HashMap<>();
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String date = simpleDateFormat.format(new Date());
		
		UUID uuid = UUID.randomUUID();
		String idFile = date+"-"+uuid.toString();
		Date dt = new Date();

		String fileName = fileStorageService.storeFile(file, idFile, dt);
		String uploadViewUrl = request.getScheme()+"://"+ request.getServerName()+ ":"+request.getServerPort()+"/upload/viewFile/"+ fileName;
		
//		Save DB 
		FileDB data = new FileDB(file.getOriginalFilename(),uploadViewUrl, file.getContentType(),file_desc,fileName, today);
		this.fileDBRepository.save(data);
		body.put("status", "success");
		body.put("data", data);
		return new ResponseEntity<>(body, HttpStatus.OK); 
	}
	
//	@PostMapping(value="/multipleFiles", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
//	private List<?> multipleFiles(@RequestPart ("files") MultipartFile[]files ,@RequestParam String file_desc, HttpServletRequest request){
//		return Arrays.asList(files)
//				.stream()
//				.map(file -> saveFile(file,file_desc , request))
//				.collect(Collectors.toList());
//	}
	
	@DeleteMapping(value="/deleteFile")
	private ResponseEntity<?> deleteFile(@RequestParam(required = false) Integer id,@RequestParam(required = false)String url,HttpServletRequest request){
		Map<String, Object> bodyDelete = new HashMap<>();
		try {
			if(id!=null) {
				if(this.fileDBRepository.findById(id).isPresent()) {
					String fileName = this.fileDBRepository.findFileName(id);
					String date = fileName.substring(0, 10);
					Date dt = new SimpleDateFormat("yyyy-MM-dd").parse(date);
					fileStorageService.deleteFile(fileName, dt);
					this.fileDBRepository.deleteById(id);
					bodyDelete.put("status", "success");
					bodyDelete.put("message", "File has been deleted successfully");
					return new ResponseEntity<>(bodyDelete,HttpStatus.OK);				
				}else {
					bodyDelete.put(status, failed );
					bodyDelete.put("message", "File null");
					return new ResponseEntity<>(bodyDelete,HttpStatus.BAD_REQUEST);
				}	
			}else {
				Integer idFileDB = this.fileDBRepository.findIdByURL(url);
				if(this.fileDBRepository.findById(idFileDB).isPresent()) {
					String fileName = this.fileDBRepository.findFileName(idFileDB);
					String date = fileName.substring(0, 10);
					Date dt = new SimpleDateFormat("yyyy-MM-dd").parse(date);
					fileStorageService.deleteFile(fileName, dt);
					this.fileDBRepository.deleteById(idFileDB);
					bodyDelete.put("status", "success");
					bodyDelete.put("message", "File has been deleted successfully");
					return new ResponseEntity<>(bodyDelete,HttpStatus.OK);				
				}else {
					bodyDelete.put(status, failed );
					bodyDelete.put("message", "File null");
					return new ResponseEntity<>(bodyDelete,HttpStatus.BAD_REQUEST);
				}	
			}
		} catch (Exception e) {
			e.printStackTrace();
			bodyDelete.put(status, failed);
			bodyDelete.put("message", "Delete file failed");
			return new ResponseEntity<>(bodyDelete,HttpStatus.BAD_REQUEST);
		}
	}
	
	private static final Logger logger = LoggerFactory.getLogger(ResponseFileUpload.class);
	private static final List<String> contentTypes = Arrays.asList("image/png", "image/jpeg");
	
	@GetMapping(value ="/viewFile/{fileName:.+}")
	public ResponseEntity<Resource> viewFile(
			@PathVariable String fileName,
			HttpServletRequest request) throws Exception{
		String date = fileName.substring(0, 10);
		System.out.println(date);
		Date dt = new SimpleDateFormat("yyyy-MM-dd").parse(date);
		Resource resource = fileStorageService.loadFileAsResource(fileName, dt);
		ResponseEntity<Resource> result = null;
		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (Exception e) {
			logger.info("Could not determine file type.");
		}
		
		// Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
        
        if(contentTypes.contains(contentType)) {
        	result = ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .body(resource); 
        } else {
        	result = ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);
        }
        
        return result;
	}

	@GetMapping(value="/get-all")
	private ResponseEntity<?> getAll( @RequestParam(defaultValue = "", required = false)String file_type,
			 @RequestParam(defaultValue = "", required = false)String file_desc,@RequestParam(defaultValue = "1") int page,
		      @RequestParam(defaultValue = "100") int size, @RequestParam (defaultValue = "id")String sort,@RequestParam (defaultValue = "asc")String order){
		Map<String, Object> body = new HashMap<>();
		Map<String, Object> data = this.fileStorageService.getAllPagesByTypeDecs(file_type, file_desc, page, size, sort, order);
		if(data.isEmpty()) {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}else {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", data);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}
	}
}
