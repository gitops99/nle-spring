package com.backendadminportalnle.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.backendadminportalnle.model.FileDB;

@Repository
public interface FileDBRepository extends JpaRepository<FileDB, Integer>{
	
	@Query(value="SELECT FILE_NAME FROM FILES WHERE ID =:id",nativeQuery = true)
	String findFileName(@Param("id")Integer id);
	
	@Query(value="SELECT ID FROM FILES WHERE URL =:url ",nativeQuery = true)
	Integer findIdByURL(@Param("url")String url);
	
	@Query(value="SELECT * FROM FILES A WHERE (A.\"TYPE\" =:file_type OR :file_type is null OR :file_type = '') " + 
	"AND (UPPER(A.FILE_DESC) LIKE '%' || UPPER(:file_desc) || '%' OR :file_desc is null OR :file_desc = '')",nativeQuery = true)
	Page<FileDB> getAllPage(@Param("file_type")String file_type,@Param("file_desc")String file_desc,Pageable pageable);
}
