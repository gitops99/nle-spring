package com.backendadminportalnle.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name="tt_news")
@SequenceGenerator(name= "seq_tt_news", sequenceName = "SEQ_ID_NEWS", initialValue=1, allocationSize = 1)
public class TT_NEWS {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tt_news")
	@Column(name="news_id")
	private Integer news_id;
	
	@Column(name="title",length = 255, nullable = false)
	private String title;
	
	@Column(name="thumbnail",length = 255, nullable = false)
	private String thumbnail;
	
	@Column(name="original_thumbnail",length = 255)
	private String original_thumbnail;
	
	@Column(name="description", columnDefinition = "CLOB")
	private String description;
	
	@Column(name="link",length = 255)
	private String link;
	
	@Column(name="slug",length = 255, nullable = false)
	private String slug;
	
	@Column(name="tag",length = 255, nullable = true)
	private String tag;
	
	@Column(name="created_by")
	private Integer created_by;
	
	@Column(name="updated_by")
	private Integer updated_by;
	
	@Column(name="created_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp created_at;
	
	@Column(name="updated_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp updated_at;
	
	@ManyToMany
	@JoinTable
	(
      name="tr_category_news",
      joinColumns={ @JoinColumn(name="news_id") },
      inverseJoinColumns={ @JoinColumn(name="category_news_id")}
	)
	private List<MST_CATEGORYNEWS> category_news;
	
	public List<MST_CATEGORYNEWS> getCategory_news() {
		return category_news;
	}

	public void setCategory_news(List<MST_CATEGORYNEWS> category_news) {
		this.category_news = category_news;
	}

	public Integer getNews_id() {
		return news_id;
	}

	public void setNews_id(Integer news_id) {
		this.news_id = news_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Integer getCreated_by() {
		return created_by;
	}

	public void setCreated_by(Integer created_by) {
		this.created_by = created_by;
	}

	public Integer getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(Integer updated_by) {
		this.updated_by = updated_by;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}

	public String getOriginal_thumbnail() {
		return original_thumbnail;
	}

	public void setOriginal_thumbnail(String original_thumbnail) {
		this.original_thumbnail = original_thumbnail;
	}

}
