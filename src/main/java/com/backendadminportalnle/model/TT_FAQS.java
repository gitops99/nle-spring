package com.backendadminportalnle.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="tt_faqs")
@SequenceGenerator(name= "seq_tt_faqs", sequenceName = "SEQ_ID_FAQS", initialValue=1, allocationSize = 1)
public class TT_FAQS {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tt_faqs")
	@Column(name="faq_id")
	private Integer faq_id;
	
	@Column(name="title",length = 255, nullable = false)
	private String title;
	
	@Column(name="description", columnDefinition = "CLOB")
	private String description;
	
	@Column(name="created_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp created_at;
	
	@Column(name="updated_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp updated_at;

	public Integer getFaq_id() {
		return faq_id;
	}

	public void setFaq_id(Integer faq_id) {
		this.faq_id = faq_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}
	
}
