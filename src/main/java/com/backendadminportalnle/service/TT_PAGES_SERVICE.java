package com.backendadminportalnle.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.backendadminportalnle.model.TT_PAGES;
import com.backendadminportalnle.repository.TT_PAGES_REPO;

@Service
public class TT_PAGES_SERVICE {
	@Autowired
	private TT_PAGES_REPO tt_PAGES_REPO;
	
	@Cacheable("dataByPageId")
	public TT_PAGES getByIdPortal(Integer pages_id) {
		return this.tt_PAGES_REPO.findbyId(pages_id);
	}
	
	public TT_PAGES getByIdCms(Integer pages_id) {
		return this.tt_PAGES_REPO.findbyId(pages_id);
	}
	
	@Cacheable("dataByPageName_pages_portal")
	public Map<String, Object> getByPageNamePortal(Integer pages_id,String page_name, 
			Integer section_number,Integer col_number,Pageable pageable){
		Map<String, Object> response = new HashMap<>();
		List<TT_PAGES> findbyPageName = new ArrayList<TT_PAGES>();
		Page<TT_PAGES> pageTuts;
		pageTuts = this.tt_PAGES_REPO.findbyPageName(pages_id,page_name, section_number, col_number, pageable);
		findbyPageName = pageTuts.getContent();
		
		response.put("data", findbyPageName);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	public Map<String, Object> getByPageNameCms(Integer pages_id,String page_name, 
			Integer section_number,Integer col_number,Pageable pageable){
		Map<String, Object> response = new HashMap<>();
		List<TT_PAGES> findbyPageName = new ArrayList<TT_PAGES>();
		Page<TT_PAGES> pageTuts;
		pageTuts = this.tt_PAGES_REPO.findbyPageName(pages_id, page_name, section_number, col_number, pageable);
		findbyPageName = pageTuts.getContent();
		
		response.put("data", findbyPageName);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	@CacheEvict(value = "dataByPageName_pages_portal", allEntries = true)
	public TT_PAGES save(TT_PAGES item, Integer user_id,Timestamp today) {
		item.setCreated_by(user_id);
		item.setUpdated_by(user_id);
		item.setCreated_at(today);
		item.setUpdated_at(today);
		item.setStatus(1);
		item.setSlug(item.getPage_name().toLowerCase().replace(" ", "-"));
		TT_PAGES data = this.tt_PAGES_REPO.save(item);
		return data;
	}
	
	@CacheEvict(value = "dataByPageName_pages_portal", allEntries = true)
	public TT_PAGES edit(Integer page_id, TT_PAGES item,Integer user_id,Timestamp today) {
		TT_PAGES tt_pages = this.tt_PAGES_REPO.findById(page_id).get();
		tt_pages.setPage_name(item.getPage_name());
		tt_pages.setSection_number(item.getSection_number());
		tt_pages.setCol_number(item.getCol_number());
		tt_pages.setTitle(item.getTitle());
		tt_pages.setContent(item.getContent());
		tt_pages.setImage(item.getImage());
		tt_pages.setVideo(item.getVideo());
		tt_pages.setUrl(item.getUrl());
		tt_pages.setRaw_content(item.getRaw_content());
		tt_pages.setList(item.getList());
		tt_pages.setUpdated_by(user_id);
		tt_pages.setUpdated_at(today);
		TT_PAGES data = this.tt_PAGES_REPO.save(tt_pages);
		return data;
	}
	
	@CacheEvict(value = "dataByPageName_pages_portal", allEntries = true)
	public Map<String, Object> delete(Integer pages_id) {
		this.tt_PAGES_REPO.deleteById(pages_id);
		Map<String, Object> response = new HashMap<>();
		response.put("status", "Success");
		response.put("message", "Page has been deleted successfully");
		return response;
	}
}
