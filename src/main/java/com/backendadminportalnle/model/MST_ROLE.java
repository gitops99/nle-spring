package com.backendadminportalnle.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="mst_role")
@SequenceGenerator(name= "seq_mst_role", sequenceName = "SEQ_ID_ROLE", initialValue=1, allocationSize = 1)
public class MST_ROLE {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "seq_mst_role")
	@Column(name="role_id")
	private Integer role_id;
	
	@Column(name="role_name",length = 255, nullable = false)
	private String role_name;

	public Integer getRole_id() {
		return role_id;
	}

	public void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}
	
}
