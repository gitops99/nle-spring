package com.backendadminportalnle.dto;

import java.util.List;
import com.backendadminportalnle.model.TT_PROFILE_PLATFORM;

public class DTOCreateMstPlatform {
	private String platform_id;

	private String platform_name;
	
	private String company;
	
	private String npwp;
	
	private String platform_url;
	
	private String thumbnail;
	
	private TT_PROFILE_PLATFORM profile_platform;
	
	private List<Integer> category_services;
	
	public DTOCreateMstPlatform() {
	}
	public DTOCreateMstPlatform(String platform_id, String platform_name, String company, String npwp,
			String platform_url, String thumbnail, 
			TT_PROFILE_PLATFORM profile_platform,List<Integer> category_services) {
	}

	public String getPlatform_id() {
		return platform_id;
	}

	public void setPlatform_id(String platform_id) {
		this.platform_id = platform_id;
	}

	public String getPlatform_name() {
		return platform_name;
	}

	public void setPlatform_name(String platform_name) {
		this.platform_name = platform_name;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public String getPlatform_url() {
		return platform_url;
	}

	public void setPlatform_url(String platform_url) {
		this.platform_url = platform_url;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public TT_PROFILE_PLATFORM getProfile_platform() {
		return profile_platform;
	}

	public void setProfile_platform(TT_PROFILE_PLATFORM profile_platform) {
		this.profile_platform = profile_platform;
	}
	public List<Integer> getCategory_services() {
		return category_services;
	}
	public void setCategory_services(List<Integer> category_services) {
		this.category_services = category_services;
	}
}
