package com.backendadminportalnle.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.backendadminportalnle.model.MST_CATEGORYSERVICE;

@Repository
public interface MST_CATEGORYSERVICE_REPO extends JpaRepository<MST_CATEGORYSERVICE, Integer>{
	@Query(value="SELECT * FROM MST_CATEGORYSERVICE "
			+ "WHERE (CATEGORYSERVICE_ID =:categoryservice_id OR :categoryservice_id = 0) "
			+ "AND (CATEGORY_NAME =:category_name OR :category_name is null OR :category_name ='')",nativeQuery = true)
	Page<MST_CATEGORYSERVICE> findAllPage(@Param("categoryservice_id")Integer categoryservice_id,@Param("category_name")String category_name,Pageable pageable);
	
	@Query(value="SELECT * FROM MST_CATEGORYSERVICE WHERE CATEGORYSERVICE_ID =:categoryservice_id ",nativeQuery = true)
	MST_CATEGORYSERVICE findbyId(@Param("categoryservice_id")Integer categoryservice_id);
}
