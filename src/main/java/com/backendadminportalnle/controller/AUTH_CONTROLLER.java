package com.backendadminportalnle.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.backendadminportalnle.dto.DTOResetPassword;
import com.backendadminportalnle.model.TT_USERS;
import com.backendadminportalnle.repository.SomethingRepository;
import com.backendadminportalnle.repository.TT_USERS_REPO;
import com.backendadminportalnle.security.LoginUser;
import com.backendadminportalnle.security.TokenProvider;
import com.backendadminportalnle.service.ResetPasswordService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.bytebuddy.utility.RandomString;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/auth")
public class AUTH_CONTROLLER {
	@Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenProvider jwtTokenUtil;
    
    @Autowired
    private TT_USERS_REPO users_REPO;
   
    @Autowired
    private ResetPasswordService resetPasswordService;
    
    @Autowired
	private BCryptPasswordEncoder bcryptEncoder;
    
    private final SomethingRepository somethingRepository;
    
    public AUTH_CONTROLLER(SomethingRepository somethingRepository) {
		this.somethingRepository = somethingRepository;
	}
    final ObjectMapper mapper = new ObjectMapper();
    
    String status = "status";
	String success = "Success";
	String failed = "Fail";
	Timestamp today = new Timestamp(System.currentTimeMillis());
    
	@PostMapping(value="/signup")
	private ResponseEntity<?> signUp(@RequestBody TT_USERS item){
		Map<String, Object> body = new HashMap<>();
		try {
			TT_USERS tt_users_username = this.users_REPO.findbyUsername(item.getUsername());
			TT_USERS tt_users_email = this.users_REPO.findbyEmail(item.getEmail());
			if(tt_users_username!=null) {
				body.put(status, failed);
				body.put("message", "Username is exist");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			}else if(tt_users_email!=null){
				body.put(status, failed);
				body.put("message", "Email is exist");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			}
			else {
				item.setPassword(bcryptEncoder.encode(item.getPassword()));
				item.setCreated_at(today);
				item.setUpdated_at(today);
				TT_USERS user = this.users_REPO.save(item);
				somethingRepository.refresh(user);
				body.put(status, success);
				body.put("data", user);
				return new ResponseEntity<>(body, HttpStatus.OK); 
			}
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody LoginUser loginUser) throws AuthenticationException, JsonProcessingException{
    	Map<String, Object> body = new HashMap<>(); 
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String token = jwtTokenUtil.generateToken(authentication);
        Date expired = jwtTokenUtil.getExpirationDateFromToken(token);
        Timestamp expired_time = new Timestamp(expired.getTime());

        // GET DATA USER LOGIN  
        String username = loginUser.getUsername();
        TT_USERS user = this.users_REPO.findbyUsername(username);
  
        Map<String, String> tokenAndExpired = new HashMap<>();
        tokenAndExpired.put("access_token", token);
        tokenAndExpired.put("expires_time", expired_time.toString());
        
        Map<String, Object> objectTokenAndExpired = new HashMap<>();
        objectTokenAndExpired.put("user", user);
        objectTokenAndExpired.put("token", tokenAndExpired);
        
        body.put("status", "success");
        body.put("data", objectTokenAndExpired);
        return new ResponseEntity<>(body, HttpStatus.OK); 
    }
    
    @PutMapping(value="/forget_password")
	private ResponseEntity<?> forgotPassword(HttpServletRequest request,@RequestBody DTOResetPassword item){
    	String token = RandomString.make(50);
    	Map<String, Object> body = new HashMap<>(); 
    	try {
			TT_USERS tt_user = this.resetPasswordService.forgetPassword(item.getEmail(), today,token);
//			String resetPasswordLink = request.getScheme()+"://"+ request.getServerName()+ ":"+request.getServerPort()+"/auth/reset_password?token=" +token;
			String linkImage = request.getScheme()+"://"+ request.getServerName()+ ":"+request.getServerPort();
			String resetPasswordLink = item.getUrl()+"/reset-password?reset_token="+token;
			resetPasswordService.sendEmail(tt_user.getFullname(),item.getEmail(), resetPasswordLink, linkImage);
			body.put(status, success);
			body.put("message", "Link reset password telah dikirimkan ke alamat email "+item.getEmail());
			return new ResponseEntity<>(body, HttpStatus.OK); 
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", "Email tidak terdaftar");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
    
    @PutMapping(value="/reset_password")
	private ResponseEntity<?> resetPassword(@RequestBody TT_USERS item, @RequestParam String token){
    	Map<String, Object> body = new HashMap<>(); 
    	try {
    		TT_USERS tt_user = this.resetPasswordService.resetPassword(item, token, today);
    		body.put(status, success);
			body.put("message", "Password berhasil diubah. Silahkan login kembali");
			return new ResponseEntity<>(body, HttpStatus.OK); 
		} catch (Exception e) {
			body.put(status, failed );
			body.put("message", "Token tidak valid");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
    }
   
}
