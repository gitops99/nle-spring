package com.backendadminportalnle.controller;


import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.backendadminportalnle.model.TT_PAGES;
import com.backendadminportalnle.repository.TT_PAGES_REPO;
import com.backendadminportalnle.repository.TT_USERS_REPO;
import com.backendadminportalnle.service.TT_PAGES_SERVICE;

@CrossOrigin(allowedHeaders = "*", origins = "*")
@RestController
@RequestMapping(value="/api/v1/")
public class TT_PAGES_CONTROLLER {
	String status = "status";
	String success = "Success";
	String failed = "Fail";
	
	Timestamp today = new Timestamp(System.currentTimeMillis());
	@Autowired
	private TT_PAGES_REPO pagesRepo;
	
	@Autowired
	private TT_USERS_REPO userRepo;
	
	@Autowired
	private TT_PAGES_SERVICE pagesService;
	
	@PostMapping(value="/pages")
	private ResponseEntity<?> save(@RequestBody TT_PAGES item){
		Map<String, Object> body = new HashMap<>();
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			Integer user_id = this.userRepo.findUserIdByUsername(auth.getName());
			TT_PAGES data = this.pagesService.save(item, user_id, today);
			body.put(status, success);
			body.put("data", data);
			return new ResponseEntity<>(body, HttpStatus.OK); 
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value="/pages/{pages_id}")
	private ResponseEntity<?> delete(@PathVariable("pages_id") Integer pages_id){
		Map<String, Object> bodyDelete = new HashMap<>();
		try {
			this.pagesService.delete(pages_id);
			bodyDelete.put(status, success);
			bodyDelete.put("message", "Page has been deleted successfully");
			return new ResponseEntity<>(bodyDelete,HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			bodyDelete.put(status, failed);
			bodyDelete.put("message", "Delete page failed");
			return new ResponseEntity<>(bodyDelete,HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value="/pages/{page_id}")
	private ResponseEntity<?> edit(@PathVariable("page_id")Integer page_id,@RequestBody TT_PAGES item){
		Map<String, Object> body = new HashMap<>();
		try {
			if (this.pagesRepo.findById(page_id).isPresent()) {
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				Integer user_id = this.userRepo.findUserIdByUsername(auth.getName());
				
				TT_PAGES data = this.pagesService.edit(page_id, item, user_id, today);
				body.put(status, success);
				body.put("data", data);
				return new ResponseEntity<>(body, HttpStatus.OK); 
			}else {
				body.put(status, failed );
				body.put("message", "Data pages news null");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);					
			}
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}	
	}
	
	@GetMapping(value="/pages")
	private ResponseEntity<?> getByPageNameCMS(@RequestParam(required = false) Integer pages_id,
										 @RequestParam String page_name,
										 @RequestParam(required = false)Integer section_number,
										 @RequestParam(required = false)Integer col_number,
										 Pageable pageable){
		Map<String, Object> body = new HashMap<>();
		section_number = section_number==null?0:section_number;
		col_number = col_number==null?0:col_number;
		pages_id = pages_id==null?0:pages_id;
		Map<String, Object> findbyPageName = pagesService.getByPageNameCms(pages_id,page_name, section_number, col_number,pageable);
		if(findbyPageName.isEmpty()) {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}else {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", findbyPageName);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/pages/{pages_id}")
	private ResponseEntity<?> getByIdCms(@PathVariable Integer pages_id){
		Map<String, Object> body = new HashMap<>();
		TT_PAGES dataFindByid = this.pagesService.getByIdCms(pages_id);
		if(dataFindByid!=null) {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", dataFindByid);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}else {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="/public/pages")
	private ResponseEntity<?> getByPageNamePublic(@RequestParam(required = false) Integer pages_id,
										 @RequestParam String page_name,
										 @RequestParam(required = false)Integer section_number,
										 @RequestParam(required = false)Integer col_number,
										 Pageable pageable){
		section_number = section_number==null?0:section_number;
		col_number = col_number==null?0:col_number;
		pages_id = pages_id==null?0:pages_id;
		Map<String, Object> body = new HashMap<>();
		
		Map<String, Object> findbyPageName = pagesService.getByPageNamePortal(pages_id,page_name, section_number, col_number,pageable);
		if(findbyPageName.isEmpty()) {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}else {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", findbyPageName);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/public/pages/{pages_id}")
	private ResponseEntity<?> getByIdPublic(@PathVariable Integer pages_id){
		Map<String, Object> body = new HashMap<>();
		TT_PAGES dataFindByid = this.pagesService.getByIdPortal(pages_id);
		if(dataFindByid!=null) {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", dataFindByid);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}else {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
}
