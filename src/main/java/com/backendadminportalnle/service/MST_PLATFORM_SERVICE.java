package com.backendadminportalnle.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.backendadminportalnle.model.MST_PLATFORM;
import com.backendadminportalnle.model.TT_PROFILE_PLATFORM;
import com.backendadminportalnle.repository.MST_PLATFORM_REPO;
import com.backendadminportalnle.repository.TT_PROFILE_PLATFORM_REPO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class MST_PLATFORM_SERVICE {
	final ObjectMapper mapper = new ObjectMapper();
	@Autowired
	private MST_PLATFORM_REPO mst_PLATFORM_REPO;
	
	@Autowired
	private TT_PROFILE_PLATFORM_REPO profilePlatformRepo;
	
	private Sort.Direction getSortDirection(String direction) {
	    if (direction.equals("asc")||direction.equals("ASC")) {
	      return Sort.Direction.ASC;
	    } else if (direction.equals("desc")||direction.equals("DESC")) {
	      return Sort.Direction.DESC;
	    }
	
	    return Sort.Direction.ASC;
	}
	
	@Cacheable("datafindById")
	public MST_PLATFORM getByidPublic(String platform_id) {
		return this.mst_PLATFORM_REPO.findbyId(platform_id);
	}
	
	public MST_PLATFORM getByidCms(String platform_id) {
		return this.mst_PLATFORM_REPO.findbyId(platform_id);
	}
		
	@Cacheable("dataByCategoryService_platform_portal")
	public Map<String, Object> getByCategoryServicePortal(Integer categoryservices_id,
			String platform_id,String platform_name, int page,int size, String sort,String order){
		Map<String, Object> response = new HashMap<>();
		List<MST_PLATFORM> findbyCategoryService = new ArrayList<MST_PLATFORM>();
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(getSortDirection(order), sort));
		Pageable pagingSort = PageRequest.of(page-1, size, Sort.by(orders));
		Page<MST_PLATFORM> pageTuts;
		pageTuts = this.mst_PLATFORM_REPO.findbyCategoryService(categoryservices_id,platform_id,platform_name,pagingSort);
		findbyCategoryService = pageTuts.getContent();
		response.put("data", findbyCategoryService);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	@Cacheable("dataByCategoryService_platform_portal")
	public Map<String, Object> getAllByPlatformPublic(String platform_id,String platform_name,int page,int size, String sort,String order){
		Map<String, Object> response = new HashMap<>();
		List<MST_PLATFORM> findbyCategoryService = new ArrayList<MST_PLATFORM>();
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(getSortDirection(order), sort));
		Pageable pagingSort = PageRequest.of(page-1, size, Sort.by(orders));
		Page<MST_PLATFORM> pageTuts;
		pageTuts = this.mst_PLATFORM_REPO.findAllPage(platform_id,platform_name,pagingSort);
		findbyCategoryService = pageTuts.getContent();
		response.put("data", findbyCategoryService);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	public Map<String, Object> getAllByPlatformCms(String platform_id,String platform_name,int page,int size, String sort,String order){
		Map<String, Object> response = new HashMap<>();
		List<MST_PLATFORM> findbyCategoryService = new ArrayList<MST_PLATFORM>();
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(getSortDirection(order), sort));
		Pageable pagingSort = PageRequest.of(page-1, size, Sort.by(orders));
		Page<MST_PLATFORM> pageTuts;
		pageTuts = this.mst_PLATFORM_REPO.findAllPage(platform_id,platform_name,pagingSort);
		findbyCategoryService = pageTuts.getContent();
		response.put("data", findbyCategoryService);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	public Map<String, Object> getByCategoryServiceCms(Integer categoryservices_id,
			String platform_id,String platform_name, int page,int size, String sort,String order){
		Map<String, Object> response = new HashMap<>();
		List<MST_PLATFORM> findbyCategoryService = new ArrayList<MST_PLATFORM>();
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(getSortDirection(order), sort));
		Pageable pagingSort = PageRequest.of(page-1, size, Sort.by(orders));
		Page<MST_PLATFORM> pageTuts;
		pageTuts = this.mst_PLATFORM_REPO.findbyCategoryService(categoryservices_id,platform_id,platform_name,pagingSort);
		findbyCategoryService = pageTuts.getContent();
		response.put("data", findbyCategoryService);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	@CacheEvict(value = "dataByCategoryService_platform_portal", allEntries = true)
	public MST_PLATFORM save(MST_PLATFORM item,Integer user_id,Timestamp today) {
		TT_PROFILE_PLATFORM profile_PLATFORM = item.getProfile_platform();
		profile_PLATFORM.setProfile_platform_id(item.getPlatform_id());
		profile_PLATFORM.setDescription(item.getProfile_platform().getDescription());
		profile_PLATFORM.setSlug(item.getProfile_platform().getSlug());
		profile_PLATFORM.setTag(item.getProfile_platform().getTag());
		profile_PLATFORM.setStatus(item.getProfile_platform().getStatus());
		profile_PLATFORM.setCreated_at(today);
		profile_PLATFORM.setCreated_by(user_id);
		profile_PLATFORM.setUpdated_at(today);
		profile_PLATFORM.setUpdated_by(user_id);
		item.setProfile_platform(profile_PLATFORM);
		MST_PLATFORM data = this.mst_PLATFORM_REPO.save(item);
		return data;
	}
	
	@CacheEvict(value = "dataByCategoryService_platform_portal", allEntries = true)
	public MST_PLATFORM edit(MST_PLATFORM item, String platform_id, Integer user_id,Timestamp today){
		MST_PLATFORM data = this.mst_PLATFORM_REPO.findById(platform_id).get();
		data.setPlatform_name(item.getPlatform_name());
		data.setCompany(item.getCompany());
		data.setNpwp(item.getNpwp());
		data.setPlatform_url(item.getPlatform_url());
		data.setThumbnail(item.getThumbnail());
		data.setCategory_services(item.getCategory_services());
		
		TT_PROFILE_PLATFORM profile_PLATFORM = this.profilePlatformRepo.findById(platform_id).get();
		profile_PLATFORM.setProfile_platform_id(data.getPlatform_id());
		profile_PLATFORM.setDescription(item.getProfile_platform().getDescription());
		profile_PLATFORM.setSlug(item.getProfile_platform().getSlug());
		profile_PLATFORM.setTag(item.getProfile_platform().getTag());
		profile_PLATFORM.setStatus(item.getProfile_platform().getStatus());
		profile_PLATFORM.setUpdated_at(today);
		profile_PLATFORM.setUpdated_by(user_id);
		data.setProfile_platform(profile_PLATFORM);
		
		MST_PLATFORM dataSave = this.mst_PLATFORM_REPO.save(data);
		return dataSave;
	}
}
