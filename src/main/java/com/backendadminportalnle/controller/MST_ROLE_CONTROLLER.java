package com.backendadminportalnle.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.backendadminportalnle.model.MST_ROLE;
import com.backendadminportalnle.repository.MST_ROLE_REPO;

@CrossOrigin(allowedHeaders = "*", origins = "*")
@RestController
@RequestMapping(value="/api/v1")
public class MST_ROLE_CONTROLLER {
	
	String status = "status";
	String success = "Success";
	String failed = "Fail";
	
	@Autowired
	private MST_ROLE_REPO mstRoleRepo;
	
	
	@PostMapping(value="/roles")
	private ResponseEntity<?> save(@RequestBody MST_ROLE item){
		Map<String, Object> body = new HashMap<>();
		try {
			List<MST_ROLE> mst_role = this.mstRoleRepo.findByRoleName(item.getRole_name());
			if(mst_role.size()>0) {
				body.put(status, failed);
				body.put("message", "Role user is exist");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			}else {
				this.mstRoleRepo.save(item);
				body.put(status, success);				
				return new ResponseEntity<>(body, HttpStatus.OK); 
			}
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	@GetMapping(value="/roles/")
	private ResponseEntity<?> findbyRoleId(@RequestParam(required = false) Integer role_id) {
		role_id = role_id==null?0:role_id;
		Map<String, Object> body = new HashMap<>();
		List<MST_ROLE> data = this.mstRoleRepo.findbyRoleId(role_id);
		try {
			body.put(status, success);
			body.put("data", data);
			return new ResponseEntity<>(body, HttpStatus.OK);
		} catch (Exception e) {
			body.put(status, failed);
			body.put("data", null);
			return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value="/roles/{role_id}")
	private ResponseEntity<?> delete(@PathVariable("role_id") Integer role_id){
		Map<String, Object> bodyDelete = new HashMap<>();
		try {
			this.mstRoleRepo.deleteById(role_id);
			bodyDelete.put(status, "Delete role successfully");
			return new ResponseEntity<>(bodyDelete, HttpStatus.OK); 
		} catch (Exception e) {
			e.printStackTrace();
			bodyDelete.put(status, failed);
			bodyDelete.put("message", "Delete role failed");
			return new ResponseEntity<>(bodyDelete,HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value="/roles/{roles_id}")
	private ResponseEntity<?> edit (@PathVariable("roles_id") Integer role_id, @RequestBody MST_ROLE item){
		Map<String, Object> body = new HashMap<>();
		try {
			if(this.mstRoleRepo.findById(role_id).isPresent()){
				MST_ROLE mst_role = this.mstRoleRepo.findById(role_id).get();
				mst_role.setRole_name(item.getRole_name());
				this.mstRoleRepo.save(mst_role);
				body.put(status, success);
				return new ResponseEntity<>(body, HttpStatus.OK);
			}else {
				body.put(status, failed);
				body.put("message", "Data role null");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);	
			}
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
}
