package com.backendadminportalnle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.backendadminportalnle.property.FileStorageProperties;
import com.backendadminportalnle.service.CustomRepositoryImpl;

@SpringBootApplication
@EnableJpaRepositories (repositoryBaseClass = CustomRepositoryImpl.class)
@EnableConfigurationProperties({FileStorageProperties.class})
@EnableCaching
public class BackendAdminPortalNleApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendAdminPortalNleApplication.class, args);
	}
}
