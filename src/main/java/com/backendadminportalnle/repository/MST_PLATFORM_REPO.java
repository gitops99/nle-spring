package com.backendadminportalnle.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.backendadminportalnle.model.MST_PLATFORM;

@Repository
public interface MST_PLATFORM_REPO extends JpaRepository<MST_PLATFORM, String>{
	@Query(value="SELECT * FROM MST_PLATFORM A " + 
			"JOIN TR_CATEGORIES_PLATFORMS B ON A.PLATFORM_ID = B.PLATFORM_ID " + 
			"JOIN MST_CATEGORYSERVICE C ON B.CATEGORYSERVICE_ID = C.CATEGORYSERVICE_ID " + 
			"WHERE (B.CATEGORYSERVICE_ID =:categoryservices_id OR :categoryservices_id= 0) " + 
			"AND (C.CATEGORYSERVICE_ID =:categoryservices_id OR :categoryservices_id= 0) " + 
			"AND (A.PLATFORM_ID =:platform_id OR :platform_id is null OR :platform_id = '') " + 
			"AND (A.PLATFORM_NAME =:platform_name OR :platform_name is null OR :platform_name = '') ",nativeQuery = true)
	Page<MST_PLATFORM> findbyCategoryService(@Param("categoryservices_id")Integer categoryservices_id,
											 @Param("platform_id")String platform_id,
											 @Param("platform_name")String platform_name,Pageable pageable);
	
	@Query(value ="SELECT * FROM MST_PLATFORM A WHERE (A.PLATFORM_ID =:platform_id OR :platform_id is null OR :platform_id = '') " + 
			"AND (A.PLATFORM_NAME =:platform_name OR :platform_name is null OR :platform_name = '') ",nativeQuery = true)
	Page<MST_PLATFORM> findAllPage(@Param("platform_id")String platform_id,
			 @Param("platform_name")String platform_name,Pageable pageable);
	
	@Query(value="SELECT * FROM MST_PLATFORM ORDER BY PLATFORM_ID",nativeQuery = true)
	public List<MST_PLATFORM> findAllByOrderByIdAsc();
	
	@Query(value="SELECT * FROM MST_PLATFORM WHERE PLATFORM_ID =:platform_id",nativeQuery = true)
	MST_PLATFORM findbyId( @Param("platform_id")String platform_id);
}
