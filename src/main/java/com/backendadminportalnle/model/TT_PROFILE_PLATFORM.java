package com.backendadminportalnle.model;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.backendadminportalnle.util.STATUS_TT_PROFILE_PLATFORM;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="tt_profile_platform")
public class TT_PROFILE_PLATFORM {
	@Id
	@Column(name="profile_platform_id")
	private String profile_platform_id;
	
	@Column(name="description", columnDefinition = "CLOB")
	private String description;
	
	@Column(name="slug",length = 255, nullable = false)
	private String slug;
	
	@Column(name="tag",length = 255, nullable = false)
	private String tag;
	
	@Column(name="status")
	private STATUS_TT_PROFILE_PLATFORM status;
	
	@Column(name="created_by")
	private Integer created_by;
	
	@Column(name="updated_by")
	private Integer updated_by;
	
	@Column(name="created_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp created_at;
	
	@Column(name="updated_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp updated_at;
	
	@JsonBackReference
	@MapsId("profile_platform_id")
	@OneToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "profile_platform_id")
	private MST_PLATFORM platform;
	
	public MST_PLATFORM getPlatform() {
		return platform;
	}

	public void setPlatform(MST_PLATFORM platform) {
		this.platform = platform;
	}

	public String getProfile_platform_id() {
		return profile_platform_id;
	}

	public void setProfile_platform_id(String profile_platform_id) {
		this.profile_platform_id = profile_platform_id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public STATUS_TT_PROFILE_PLATFORM getStatus() {
		return status;
	}

	public void setStatus(STATUS_TT_PROFILE_PLATFORM status) {
		this.status = status;
	}

	public Integer getCreated_by() {
		return created_by;
	}

	public void setCreated_by(Integer created_by) {
		this.created_by = created_by;
	}

	public Integer getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(Integer updated_by) {
		this.updated_by = updated_by;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}
	
}
