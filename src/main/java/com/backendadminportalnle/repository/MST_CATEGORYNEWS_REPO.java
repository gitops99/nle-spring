package com.backendadminportalnle.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.backendadminportalnle.model.MST_CATEGORYNEWS;

@Repository
public interface MST_CATEGORYNEWS_REPO extends JpaRepository<MST_CATEGORYNEWS, Integer>{
	@Query(value="SELECT * FROM mst_categorynews WHERE (category_news_id =:category_news_id OR :category_news_id = 0 )",nativeQuery = true)
	Page<MST_CATEGORYNEWS> findAllPage(@Param("category_news_id")Integer category_news_id,Pageable pageable);
	
	@Query(value="SELECT * FROM mst_categorynews WHERE category_news_id = :category_news_id ",nativeQuery = true)
	MST_CATEGORYNEWS findByid(@Param("category_news_id")Integer category_news_id);
}
