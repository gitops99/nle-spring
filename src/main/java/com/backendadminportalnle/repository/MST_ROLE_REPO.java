package com.backendadminportalnle.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.backendadminportalnle.model.MST_ROLE;

@Repository
public interface MST_ROLE_REPO extends JpaRepository<MST_ROLE, Integer>{
	
	@Query("select r from MST_ROLE r where r.role_name=:role_name")
	List<MST_ROLE> findByRoleName(@Param("role_name")String role_name);
	
	@Query(value="SELECT * FROM MST_ROLE WHERE (ROLE_ID =:role_id OR :role_id=0)",nativeQuery = true)
	List<MST_ROLE> findbyRoleId(@Param("role_id")Integer role_id);
}