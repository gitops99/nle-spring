package com.backendadminportalnle.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="tt_pages")
@SequenceGenerator(name= "seq_tt_pages", sequenceName = "SEQ_ID_PAGES", initialValue=1, allocationSize = 1)
public class TT_PAGES {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tt_pages")
	@Column(name="pages_id")
	private Integer pages_id;
	
	@Column(name="page_name",length = 255, nullable = false)
	private String page_name;
	
	@Column(name="section_number", nullable = false)
	private Integer section_number;
	
	@Column(name="col_number", nullable = false)
	private Integer col_number;
	
	@Column(name="content", columnDefinition = "CLOB")
	private String content;
	
	@Column(name="title", columnDefinition = "CLOB")
	private String title;
	
	@Column(name="image", columnDefinition = "CLOB")
	private String image;
	
	@Column(name="video", columnDefinition = "CLOB")
	private String video;
	
	@Column(name="raw_content", columnDefinition = "CLOB")
	private String raw_content;
	
	@Column(name="list", columnDefinition = "CLOB")
	private String list;
	
	@Column(name="slug",length = 255, nullable = false)
	private String slug;
	
	@Column(name="url", columnDefinition = "CLOB")
	private String url;
	
	@Column(name="status", columnDefinition = "integer default 1",nullable = false)
	private Integer status;
	
	@Column(name="created_by")
	private Integer created_by;
	
	@Column(name="updated_by")
	private Integer updated_by;
	
	@Column(name="created_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp created_at;
	
	@Column(name="updated_at")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Timestamp updated_at;

	public Integer getPages_id() {
		return pages_id;
	}

	public void setPages_id(Integer pages_id) {
		this.pages_id = pages_id;
	}

	public String getPage_name() {
		return page_name;
	}

	public void setPage_name(String page_name) {
		this.page_name = page_name;
	}

	public Integer getSection_number() {
		return section_number;
	}

	public void setSection_number(Integer section_number) {
		this.section_number = section_number;
	}

	public Integer getCol_number() {
		return col_number;
	}

	public void setCol_number(Integer col_number) {
		this.col_number = col_number;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public String getRaw_content() {
		return raw_content;
	}

	public void setRaw_content(String raw_content) {
		this.raw_content = raw_content;
	}

	public String getList() {
		return list;
	}

	public void setList(String list) {
		this.list = list;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getCreated_by() {
		return created_by;
	}

	public void setCreated_by(Integer created_by) {
		this.created_by = created_by;
	}

	public Integer getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(Integer updated_by) {
		this.updated_by = updated_by;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}

}
