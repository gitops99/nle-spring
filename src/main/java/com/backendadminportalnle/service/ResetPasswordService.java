package com.backendadminportalnle.service;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.backendadminportalnle.model.TT_USERS;
import com.backendadminportalnle.repository.TT_USERS_REPO;

@Service
public class ResetPasswordService {
	
	@Autowired
	private TT_USERS_REPO users_REPO;
	
	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;
	
	private JavaMailSender javaMailSender;
	private TemplateEngine templateEngine;
	public ResetPasswordService(JavaMailSender javaMailSender, TemplateEngine templateEngine) {
		this.javaMailSender = javaMailSender;
		this.templateEngine = templateEngine;
	}
	
	public void sendEmail(String Fullname,String recipientEmail, String link,String linkImage) throws MessagingException, UnsupportedEncodingException {
		String subject = "Here's the link to reset your password";
		Context context = new Context();
		context.setVariable("recipientEmail", Fullname);
		context.setVariable("link", link);
		context.setVariable("linkImage", linkImage);
		String htmlPage = templateEngine.process("verification",context);
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");
//		String htmlPage = content;
		
		
		helper.setText(htmlPage, true);
		helper.setTo(recipientEmail);
		helper.setSubject(subject);
		javaMailSender.send(mimeMessage);
	}
	
	public TT_USERS forgetPassword(String email,Timestamp today,String token) {
		TT_USERS userExisting = this.users_REPO.findbyEmail(email);
		userExisting.setReset_token(token);
		userExisting.setUpdated_at(today);
		TT_USERS save = this.users_REPO.save(userExisting);
		return save;
	}
	
	public TT_USERS resetPassword(TT_USERS item,String reset_token,Timestamp today) {
		TT_USERS userExisting = this.users_REPO.findByResetToken(reset_token);
		userExisting.setUpdated_at(today);
		userExisting.setPassword(bcryptEncoder.encode(item.getPassword()));
		userExisting.setReset_token(null);
		TT_USERS save = this.users_REPO.save(userExisting);
		return save;
	}
}
