package com.backendadminportalnle.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.backendadminportalnle.model.TT_NEWS;

@Repository
public interface TT_NEWS_REPO extends JpaRepository<TT_NEWS, Integer>{
	@Query(value="SELECT * FROM TT_NEWS A " + 
			"JOIN TR_CATEGORY_NEWS B ON A.NEWS_ID = B.NEWS_ID " + 
			"WHERE (B.CATEGORY_NEWS_ID IN (:categorynews_id)) " + 
			"AND (A.NEWS_ID =:news_id OR :news_id=0) " + 
			"AND (UPPER(A.TITLE) LIKE '%' || UPPER(:title) ||'%' OR :title IS NULL OR :title = '') ",nativeQuery = true)
	Page<TT_NEWS> findbyCategoryNewsId(@Param("categorynews_id")List<Integer> categorynews_id,@Param("news_id")Integer news_id,@Param("title")String title,Pageable pageable);
	
	@Query(value="SELECT * FROM TT_NEWS A WHERE (A.NEWS_ID =:news_id OR :news_id=0) " + 
			"AND (UPPER(A.TITLE) LIKE '%' || UPPER(:title) ||'%' OR :title IS NULL OR :title = '') ",nativeQuery = true)
	Page<TT_NEWS> findAllPage(@Param("news_id")Integer news_id,@Param("title")String title,Pageable pageable);
	
	@Query(value="SELECT * FROM TT_NEWS WHERE NEWS_ID =:news_id",nativeQuery = true)
	TT_NEWS findbyId(@Param("news_id")Integer news_id);
	
	@Query(value="SELECT * FROM (SELECT mc.CATEGORY_NEWS_ID ,mc.CATEGORY_NAME , tn.* , ROW_NUMBER ()OVER(PARTITION BY mc.CATEGORY_NEWS_ID ORDER BY tn.CREATED_AT DESC) AS RN " + 
			"FROM MST_CATEGORYNEWS mc JOIN TR_CATEGORY_NEWS tcn ON mc.CATEGORY_NEWS_ID = tcn.CATEGORY_NEWS_ID " + 
			"JOIN TT_NEWS tn ON tcn.NEWS_ID = tn.NEWS_ID) sub " + 
			"WHERE RN <= 2",nativeQuery = true)
	List<TT_NEWS> find2BycategoryNews();
	
	@Query(value="SELECT tn.THUMBNAIL FROM TT_NEWS tn WHERE NEWS_ID =:news_id",nativeQuery = true)
	String findThumbnail(@Param("news_id")Integer news_id);
	
	@Query(value="SELECT tn.ORIGINAL_THUMBNAIL FROM TT_NEWS tn WHERE NEWS_ID =:news_id",nativeQuery = true)
	String findOriginalThumbnail(@Param("news_id")Integer news_id);
}
