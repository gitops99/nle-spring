package com.backendadminportalnle.controller;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.backendadminportalnle.model.TT_FAQS;
import com.backendadminportalnle.repository.TT_FAQS_REPO;
import com.backendadminportalnle.service.TT_FAQS_SERVICE;

@CrossOrigin(allowedHeaders = "*", origins = "*")
@RestController
@RequestMapping(value="/api/v1/")
public class TT_FAQS_CONTROLLER {
	String status = "status";
	String success = "Success";
	String failed = "Fail";
	Timestamp today = new Timestamp(System.currentTimeMillis());
	
	@Autowired
	private TT_FAQS_REPO tt_FAQS_REPO;
	
	@Autowired
	private TT_FAQS_SERVICE ttFaqsService;
	
	@PostMapping(value="/faqs")
	private ResponseEntity<?> save(@RequestBody TT_FAQS item){
		Map<String, Object> body = new HashMap<>();
		try {
			TT_FAQS data = this.ttFaqsService.save(item, today);
			body.put(status, success);
			body.put("data", data);
			return new ResponseEntity<>(body, HttpStatus.OK); 
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value="/faqs/{faq_id}")
	private ResponseEntity<?> delete(@PathVariable("faq_id") Integer faq_id){
		Map<String, Object> bodyDelete = new HashMap<>();
		try {
			this.ttFaqsService.delete(faq_id);
			bodyDelete.put(status, success);
			bodyDelete.put("message", "Faqs has been deleted successfully");
			return new ResponseEntity<>(bodyDelete,HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			bodyDelete.put(status, failed);
			bodyDelete.put("message", "Delete faqs failed");
			return new ResponseEntity<>(bodyDelete,HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value="/faqs/{faq_id}")
	private ResponseEntity<?> edit(@PathVariable("faq_id") Integer faq_id, @RequestBody TT_FAQS item){
		Map<String, Object> body = new HashMap<>();
		try {
			if(this.tt_FAQS_REPO.findById(faq_id).isPresent()) {
				TT_FAQS tt_faqs = this.ttFaqsService.edit(faq_id, item, today);
				body.put(status, success);
				body.put("data", tt_faqs);
				return new ResponseEntity<>(body, HttpStatus.OK); 
			}else {
				body.put(status, failed );
				body.put("message", "Data Faqs null");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);					
			}
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="/faqs")
	private ResponseEntity<?> getFaqsCms(@RequestParam(required = false)Integer faqs_id,@RequestParam(defaultValue = "1") int page,
		      @RequestParam(defaultValue = "10") int size, @RequestParam (defaultValue = "faq_id")String sort,@RequestParam (defaultValue = "asc")String order){
		Map<String, Object> body = new HashMap<>();
		faqs_id = faqs_id==null?0:faqs_id;
		Map<String, Object> data = this.ttFaqsService.getTTFaqsCms(faqs_id,page,size,sort,order);
		if(data.isEmpty()) {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}else {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", data);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/faqs/{faqs_id}")
	private ResponseEntity<?> getByidCms(@PathVariable("faqs_id")Integer faqs_id){
		Map<String, Object> body = new HashMap<>();
		TT_FAQS dataFindbyId = this.ttFaqsService.getByIdCms(faqs_id);
		if(dataFindbyId!=null) {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", dataFindbyId);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}else {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="/public/faqs")
	private ResponseEntity<?> getFaqsPublic(@RequestParam(required = false)Integer faqs_id,@RequestParam(defaultValue = "1") int page,
		      @RequestParam(defaultValue = "10") int size, @RequestParam (defaultValue = "faq_id")String sort,@RequestParam (defaultValue = "asc")String order){
		Map<String, Object> body = new HashMap<>();
		faqs_id = faqs_id==null?0:faqs_id;
		Map<String, Object> data = this.ttFaqsService.getTTFaqsPublic(faqs_id,page,size,sort,order);
		if(data.isEmpty()) {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}else {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", data);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/public/faqs/{faqs_id}")
	private ResponseEntity<?> getByidPublic(@PathVariable("faqs_id")Integer faqs_id){
		Map<String, Object> body = new HashMap<>();
		TT_FAQS dataFindbyId = this.ttFaqsService.getByIdPublic(faqs_id);
		if(dataFindbyId!=null) {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", dataFindbyId);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}else {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
}
