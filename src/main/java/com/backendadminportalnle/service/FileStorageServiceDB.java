package com.backendadminportalnle.service;

import java.sql.Timestamp;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.backendadminportalnle.model.FileDB;
import com.backendadminportalnle.repository.FileDBRepository;

@Service
public class FileStorageServiceDB {
	@Autowired
	private FileDBRepository fileDBRepository;
	
	Timestamp today = new Timestamp(System.currentTimeMillis());
	
//	public FileDB store(MultipartFile file) throws Exception{
//		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
//		FileDB data = new FileDB(fileName, file.getContentType(),today);
//		return this.fileDBRepository.save(data);
//	}
	
	public FileDB getFile(Integer id) {
		return fileDBRepository.findById(id).get();
	}
	  
	public Stream<FileDB> getAllFiles() {
		return fileDBRepository.findAll().stream();
	}
}
