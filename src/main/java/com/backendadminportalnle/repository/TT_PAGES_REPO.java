package com.backendadminportalnle.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.backendadminportalnle.model.TT_PAGES;

public interface TT_PAGES_REPO extends JpaRepository<TT_PAGES, Integer>{
	@Query(value="SELECT * FROM TT_PAGES tp "
			+ "WHERE tp.PAGE_NAME =:page_name AND (tp.PAGES_ID=:pages_id OR :pages_id=0) AND (SECTION_NUMBER =:section_number OR :section_number= 0) AND (COL_NUMBER =:col_number OR :col_number = 0) ORDER BY SECTION_NUMBER , COL_NUMBER ",nativeQuery = true)
	Page<TT_PAGES> findbyPageName(@Param("pages_id") Integer pages_id,
								  @Param("page_name")String page_name, 
								  @Param("section_number")Integer section_number,
								  @Param("col_number")Integer col_number,
								  Pageable pageable);
	
	@Query(value="SELECT * FROM TT_PAGES ",nativeQuery=true)
	Page<TT_PAGES> findAllPage(Pageable pageable);
	
	@Query(value="SELECT * FROM TT_PAGES WHERE PAGES_ID =:pages_id",nativeQuery = true)
	TT_PAGES findbyId(@Param("pages_id")Integer pages_id);
}
