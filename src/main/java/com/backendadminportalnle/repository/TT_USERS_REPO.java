package com.backendadminportalnle.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.backendadminportalnle.model.TT_USERS;

@Repository
public interface TT_USERS_REPO extends JpaRepository<TT_USERS, Integer>{
	void refresh(TT_USERS t);
	
	@Query("select u from TT_USERS u where u.username=:username")
	TT_USERS findbyUsername(@Param("username")String username);
	
	@Query("select u from TT_USERS u where u.username=:username and u.password=:password")
	TT_USERS findbyUsernamePassword(@Param("username")String username,@Param("password")String password);
	
	@Query("select u from TT_USERS u where u.fullname=:fullname")
	List<TT_USERS> findbyFullname(@Param("fullname")String fullname);
	
	Boolean existsByUsername(String username);
	
	@Query(value="SELECT * FROM TT_USERS WHERE (ROLE_ID =:role_id OR :role_id = 0) AND (USERS_ID =:users_id OR :users_id = 0)",nativeQuery = true)
	List<TT_USERS> findbyRoleId(@Param("role_id")Integer role_id,@Param("users_id")Integer users_id);
	
	@Query(value="SELECT USERS_ID FROM TT_USERS WHERE USERNAME =:username",nativeQuery = true)
	Integer findUserIdByUsername(@Param("username")String username);
	
	@Query("select u from TT_USERS u where u.email=:email")
	TT_USERS findbyEmail(@Param("email") String email);
	
	@Query(value ="SELECT * FROM TT_USERS WHERE RESET_TOKEN =:reset_token",nativeQuery = true)
	TT_USERS findByResetToken(@Param("reset_token")String reset_token);
	
}