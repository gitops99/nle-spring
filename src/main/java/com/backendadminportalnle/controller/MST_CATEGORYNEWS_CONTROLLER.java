package com.backendadminportalnle.controller;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.backendadminportalnle.model.MST_CATEGORYNEWS;
import com.backendadminportalnle.repository.MST_CATEGORYNEWS_REPO;
import com.backendadminportalnle.service.MST_CATEGORYNEWS_SERVICE;

@CrossOrigin(allowedHeaders = "*", origins = "*")
@RestController
@RequestMapping(value="/api/v1/")
public class MST_CATEGORYNEWS_CONTROLLER {
	String status = "status";
	String success = "Success";
	String failed = "Fail";
	Timestamp today = new Timestamp(System.currentTimeMillis());
	@Autowired
	private MST_CATEGORYNEWS_REPO categorynews_REPO;
	
	@Autowired
	private MST_CATEGORYNEWS_SERVICE categoryService;
	
	@PostMapping(value="/categorynews")
	private ResponseEntity<?> save (@RequestBody MST_CATEGORYNEWS item){
		Map<String, Object> body = new HashMap<>();
		try {
			MST_CATEGORYNEWS data = this.categoryService.save(item, today);
			body.put(status, success);
			body.put("data", data);
			return new ResponseEntity<>(body, HttpStatus.OK); 
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value="/categorynews/{categorynews_id}")
	private ResponseEntity<?> edit(@PathVariable("categorynews_id") Integer categorynews_id, @RequestBody MST_CATEGORYNEWS item){
		Map<String, Object> body = new HashMap<>();
		try {
			if (this.categorynews_REPO.findById(categorynews_id).isPresent()) {
				MST_CATEGORYNEWS data = this.categoryService.edit(categorynews_id, item, today);
				
				body.put(status, success);
				body.put("message", "Category News has been updated successfully");
				return new ResponseEntity<>(body, HttpStatus.OK); 
			}else {
				body.put(status, failed );
				body.put("message", "Data category news null");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);			
			}			
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@DeleteMapping(value="/categorynews/{categorynews_id}")
	private ResponseEntity<?> delete (@PathVariable("categorynews_id") Integer categorynews_id){
		Map<String, Object> bodyDelete = new HashMap<>();
		try {
			this.categoryService.delete(categorynews_id);
			bodyDelete.put(status, success);
			bodyDelete.put("message", "Category News has been deleted successfully");
			return new ResponseEntity<>(bodyDelete,HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			bodyDelete.put(status, failed);
			bodyDelete.put("message", "Delete category news failed");
			return new ResponseEntity<>(bodyDelete,HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="/categorynews")
	private ResponseEntity<?> getAllCms(@RequestParam(required = false) Integer category_news_id,
			@RequestParam(defaultValue = "1") int page,
		      @RequestParam(defaultValue = "10") int size, @RequestParam (defaultValue = "category_news_id")String sort,@RequestParam (defaultValue = "asc")String order){
		Map<String, Object> body = new HashMap<>();
		category_news_id = category_news_id==null?0:category_news_id;
		Map<String, Object> data = this.categoryService.findAllCms(category_news_id,page,size,sort,order);
		if(data.isEmpty()) {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}else {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", data);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/categorynews/{category_news_id}")
	private ResponseEntity<?> getByIdCms(@PathVariable("category_news_id")Integer category_news_id){
		Map<String, Object> body = new HashMap<>();
		MST_CATEGORYNEWS dataFindbyId = this.categoryService.findByIdCms(category_news_id);
		if(dataFindbyId!=null) {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", dataFindbyId);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}else {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="/public/categorynews")
	private ResponseEntity<?> getAllPublic(	@RequestParam(required = false) Integer category_news_id,
			@RequestParam(defaultValue = "1") int page,
		      @RequestParam(defaultValue = "10") int size, @RequestParam (defaultValue = "category_news_id")String sort,@RequestParam (defaultValue = "asc")String order){
		Map<String, Object> body = new HashMap<>();
		category_news_id = category_news_id==null?0:category_news_id;
		Map<String, Object> data = this.categoryService.findAllPortal(category_news_id,page,size,sort,order);
		if(data.isEmpty()) {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}else {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", data);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/public/categorynews/{category_news_id}")
	private ResponseEntity<?> getByIdPortal(@PathVariable("category_news_id")Integer category_news_id){
		Map<String, Object> body = new HashMap<>();
		MST_CATEGORYNEWS dataFindbyId = this.categoryService.findByIdPortal(category_news_id);
		if(dataFindbyId!=null) {
			body.put(status,HttpStatus.OK.value());
			body.put("message", success);
			body.put("data", dataFindbyId);
			return new ResponseEntity<>(body,HttpStatus.OK);
		}else {
			body.put(status, HttpStatus.BAD_REQUEST.value());
			body.put("message", "Data tidak ditemukan");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
}
