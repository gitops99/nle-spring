package com.backendadminportalnle.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.backendadminportalnle.model.MST_CATEGORYNEWS;
import com.backendadminportalnle.repository.MST_CATEGORYNEWS_REPO;

@Service
public class MST_CATEGORYNEWS_SERVICE {
	@Autowired
	private MST_CATEGORYNEWS_REPO categorynews_REPO;
	
	private Sort.Direction getSortDirection(String direction) {
	    if (direction.equals("asc")||direction.equals("ASC")) {
	      return Sort.Direction.ASC;
	    } else if (direction.equals("desc")||direction.equals("DESC")) {
	      return Sort.Direction.DESC;
	    }
	
	    return Sort.Direction.ASC;
	}
	
	@Cacheable("dataByCategoryNewsId")
	public MST_CATEGORYNEWS findByIdPortal(Integer categorynews_id) {
		return this.categorynews_REPO.findByid(categorynews_id);
	}
	
	public MST_CATEGORYNEWS findByIdCms(Integer categorynews_id) {
		return this.categorynews_REPO.findByid(categorynews_id);
	}
	
	@Cacheable("dataAll_CategoryNews_portal")
	public Map<String, Object> findAllPortal(Integer category_news_id,int page,int size, String sort,String order){
		Map<String, Object> response = new HashMap<>();
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(getSortDirection(order), sort));
		List<MST_CATEGORYNEWS> data = new ArrayList<MST_CATEGORYNEWS>();
		Pageable pagingSort = PageRequest.of(page-1, size, Sort.by(orders));
		Page<MST_CATEGORYNEWS> pageTuts;
		pageTuts = this.categorynews_REPO.findAllPage(category_news_id,pagingSort);
		data = pageTuts.getContent();
		
		response.put("data", data);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	public Map<String, Object> findAllCms(Integer category_news_id,int page,int size, String sort,String order){
		Map<String, Object> response = new HashMap<>();
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order(getSortDirection(order), sort));
		List<MST_CATEGORYNEWS> data = new ArrayList<MST_CATEGORYNEWS>();
		Pageable pagingSort = PageRequest.of(page-1, size, Sort.by(orders));
		Page<MST_CATEGORYNEWS> pageTuts;
		pageTuts = this.categorynews_REPO.findAllPage(category_news_id,pagingSort);
		data = pageTuts.getContent();
		
		response.put("data", data);
		response.put("totalItems",pageTuts.getTotalElements());
		response.put("totalPages",pageTuts.getTotalPages());
		response.put("currentPage",pageTuts.getNumber()+1);
		return response;
	}
	
	@CacheEvict(value = "dataAll_CategoryNews_portal", allEntries = true)
	public MST_CATEGORYNEWS save(MST_CATEGORYNEWS item, Timestamp today) {
		item.setCreated_at(today);
		item.setUpdated_at(today);
		MST_CATEGORYNEWS data = this.categorynews_REPO.save(item);
		return data;
	}
	
	@CacheEvict(value = "dataAll_CategoryNews_portal", allEntries = true)
	public MST_CATEGORYNEWS edit(Integer categorynews_id, MST_CATEGORYNEWS item, Timestamp today) {
		MST_CATEGORYNEWS data = this.categorynews_REPO.findById(categorynews_id).get();
		data.setCategory_name(item.getCategory_name());
		data.setUpdated_at(today);
		MST_CATEGORYNEWS resp = this.categorynews_REPO.save(data);
		return resp;
	}
	
	@CacheEvict(value = "dataAll_CategoryNews_portal", allEntries = true)
	public Map<String, Object> delete(Integer categorynews_id) {
		this.categorynews_REPO.deleteById(categorynews_id);
		Map<String, Object> response = new HashMap<>();
		response.put("status", "Success");
		response.put("message", "Category News has been deleted successfully");
		return response;
	}
}
