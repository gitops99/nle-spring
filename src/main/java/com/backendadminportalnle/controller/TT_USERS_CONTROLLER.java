package com.backendadminportalnle.controller;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.backendadminportalnle.model.TT_USERS;
import com.backendadminportalnle.repository.SomethingRepository;
import com.backendadminportalnle.repository.TT_USERS_REPO;
import com.backendadminportalnle.security.UserServiceImpl;

@CrossOrigin(allowedHeaders = "*", origins = "*")
@RestController
@RequestMapping(value="/api/v1")
public class TT_USERS_CONTROLLER {
	
	String status = "status";
	String success = "Success";
	String failed = "Fail";
	Timestamp today = new Timestamp(System.currentTimeMillis());
	@Autowired
	private TT_USERS_REPO userRepo;
	
	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;
	
	@Autowired
	private UserServiceImpl serviceImpl;
	
	private final SomethingRepository somethingRepository;
	
	public TT_USERS_CONTROLLER(SomethingRepository somethingRepository) {
		this.somethingRepository = somethingRepository;
	}
	
	@PostMapping(value="/users")
	private ResponseEntity<Map<String, ?>> save(@RequestBody TT_USERS item){
		Map<String, Object> body = new HashMap<>();
		try {
			TT_USERS tt_users_username = this.userRepo.findbyUsername(item.getUsername());
			TT_USERS tt_users_email = this.userRepo.findbyEmail(item.getEmail());
			if(tt_users_username!=null) {
				body.put(status, failed);
				body.put("message", "Username is exist");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			}else if(tt_users_email!=null){
				body.put(status, failed);
				body.put("message", "Email is exist");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			}
			else {
				item.setPassword(bcryptEncoder.encode(item.getPassword()));
				item.setCreated_at(today);
				item.setUpdated_at(today);
				TT_USERS user = this.userRepo.save(item);
				somethingRepository.refresh(user);
				body.put(status, success);
				body.put("data", user);
				return new ResponseEntity<>(body, HttpStatus.OK); 
			}
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(value="/users/")
	private ResponseEntity<?> getByRoleId(@RequestParam(required = false) Integer role_id,@RequestParam(required = false) Integer users_id){
		role_id = role_id==null?0:role_id;
		users_id = users_id==null?0:users_id;
		Map<String, Object> body = new HashMap<>();
		List<TT_USERS> findbyRoleId = this.userRepo.findbyRoleId(role_id,users_id);
		try {
			body.put(status, success);
			body.put("data", findbyRoleId);
			return new ResponseEntity<>(body, HttpStatus.OK);
		} catch (Exception e) {
			body.put(status, failed);
			body.put("data", null);
			return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value="/users/{user_id}")
	private ResponseEntity<?> delete(@PathVariable("user_id") Integer users_id){
		Map<String, Object> bodyDelete = new HashMap<>();
		try {
			this.userRepo.deleteById(users_id);
			bodyDelete.put(status, success);
			bodyDelete.put("message", "User has been deleted successfully");
			return new ResponseEntity<>(bodyDelete,HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			bodyDelete.put(status, failed);
			bodyDelete.put("message", "Delete users failed");
			return new ResponseEntity<>(bodyDelete,HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value="/users/{user_id}")
	private ResponseEntity<?> edit(@PathVariable("user_id") Integer user_id,@RequestBody TT_USERS item){
		Map<String, Object> body = new HashMap<>();
		try {
			if(this.userRepo.findById(user_id).isPresent()) {
				TT_USERS data = this.userRepo.findById(user_id).get();
				data.setUpdated_at(today);
				data.setRole_id(item.getRole_id());
				data.setUsername(item.getUsername());
				data.setEmail(item.getEmail());
				data.setFullname(item.getFullname());
				data.setNpwp(item.getNpwp());
				TT_USERS dataSave = this.userRepo.save(data);
				body.put(status, success);
				body.put("data", dataSave);
				return new ResponseEntity<>(body, HttpStatus.OK);
			}else {
				body.put(status, failed );
				body.put("message", "Data user null");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);	
			}
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value="/update/approved/{user_id}")
	private ResponseEntity<?> editIsApproved(@PathVariable("user_id")Integer user_id,@RequestBody TT_USERS item){
		Map<String, Object> body = new HashMap<>();
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();
			for(GrantedAuthority e:authorities) {
				String ROLE = e.getAuthority();
				if(ROLE.equals("SUPERADMIN")||ROLE.equals("ADMIN")) {
					if(this.userRepo.findById(user_id).isPresent()) {
						TT_USERS data = this.userRepo.findById(user_id).get();
						data.setUpdated_at(today);
						data.setIs_approved(item.getIs_approved());
						TT_USERS dataSave = this.userRepo.save(data);
						body.put(status, success);
						body.put("data", dataSave);
						return new ResponseEntity<>(body, HttpStatus.OK);
					}else {
						body.put(status, failed );
						body.put("message", "Data user null");
						return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);	
					}
				}
			}
			body.put(status, failed );
			body.put("message", "User not allowed");
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);	
		} catch (Exception e) {
			e.printStackTrace();
			body.put(status, failed );
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}
}
